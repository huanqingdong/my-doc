## [SQL access](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/xpack-sql.html)

### [Overview](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/sql-overview.html)

为什么使用ElasticSearch Sql

* 原生整合
* 不需要外部部件
* 轻量高效

### [Getting Started with SQL](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/sql-getting-started.html)

```nginx
# 插入测试数据
PUT /library/book/_bulk?refresh
{"index":{"_id": "Leviathan Wakes"}}
{"name": "Leviathan Wakes", "author": "James S.A. Corey", "release_date": "2011-06-02", "page_count": 561}
{"index":{"_id": "Hyperion"}}
{"name": "Hyperion", "author": "Dan Simmons", "release_date": "1989-05-26", "page_count": 482}
{"index":{"_id": "Dune"}}
{"name": "Dune", "author": "Frank Herbert", "release_date": "1965-06-01", "page_count": 604}
# SQl查询
POST /_sql?format=txt
{
    "query": "SELECT * FROM library WHERE release_date < '2000-01-01'"
}

kibaba监控docker中的ES,无法获取cpu信息

```

### [SQL REST API](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/sql-rest.html)

SQL查询可用显示格式

| **format**         | **Accept HTTP header**      | **Description**                                              |
| ------------------ | --------------------------- | ------------------------------------------------------------ |
| **Human Readable** |                             |                                                              |
| `csv`              | `text/csv`                  | [Comma-separated values](https://en.wikipedia.org/wiki/Comma-separated_values) |
| `json`             | `application/json`          | [JSON](https://www.json.org/) (JavaScript Object Notation) human-readable format |
| `tsv`              | `text/tab-separated-values` | [Tab-separated values](https://en.wikipedia.org/wiki/Tab-separated_values) |
| `txt`              | `text/plain`                | CLI-like representation                                      |
| `yaml`             | `application/yaml`          | [YAML](https://en.wikipedia.org/wiki/YAML) (YAML Ain’t Markup Language) human-readable format |
| **Binary Formats** |                             |                                                              |
| `cbor`             | `application/cbor`          | [Concise Binary Object Representation](http://cbor.io/)      |
| `smile`            | `application/smile`         | [Smile](https://en.wikipedia.org/wiki/Smile_(data_interchange_format)) binary data format similar to CBOR |

### [SQL Translate API](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/sql-translate.html)

```shell
POST /_sql/translate
{
    "query": "SELECT * FROM library ORDER BY page_count DESC",
    "fetch_size": 10
}
```

