* 创建索引
```markdown
PUT twitter_t
{
  "settings" : {
    "number_of_shards" : 2,   
    "number_of_replicas" : 2 
  },
  "mappings" : {
    "_doc" : {
      "properties" : {
        "field1" : { "type" : "text" }
      }
    }
  }
}
```
* 查看索引
```markdown
# 查看索引,可使用通配符(*,_all),展示json格式
GET twitter
# 查看索引,展示csv格式
GET _cat/indices?v
```
* 删除索引
```markdown
DELETE twitter
```
* 打开关闭索引
```markdown
# 关闭索引,关闭后查询:index_closed_exception
POST /twitter/_close
# 打开索引
POST /twitter/_open
```
* 收缩索引
```markdown
# 创建原索引
PUT twitter
{
  "settings" : {
    "number_of_shards" : 4,   
    "number_of_replicas" : 2 
  },
  "mappings" : {
    "_doc" : {
      "properties" : {
        "field1" : { "type" : "text" }
      }
    }
  }
}
# 放入一个文档
PUT twitter/_doc/1
{
    "user" : "kimchy",
    "post_date" : "2009-11-15T14:12:12",
    "message" : "trying out Elasticsearch"
}
# 设置索引只读
PUT /twitter/_settings
{
  "settings": {
    "index.routing.allocation.require._name": "twitter",
    "index.blocks.write": true 
  }
}
# 收缩操作,设置目标shard数为2
POST twitter/_shrink/twitter_t?copy_settings=true
{
  "settings": {
    "index.routing.allocation.require._name": null, 
    "index.blocks.write": null,
    "index.number_of_shards": 2
  }
}
```
## [Split Index](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/indices-split-index.html)

> 索引切分
>
> split index API允许切分一个已存在的索引到一个新的索引,在新索引中每个原始主分片会被切分成两个或多个主分片

```nginx
# 创建用于测试的索引,number_of_routing_shards参数必须在索引创建时指定,后续指定无效.
# 1. 索引分割后的分片数必须是原分片数的整数倍
# 2. number_of_routing_shards必须是分割后分片数的整数倍
# 3. 7.x版本之后不需要设置此参数
PUT split_test_index
{
    "settings": {
        "index.number_of_shards" : 1,
        "index.number_of_routing_shards" : 2 
    }
}
# 查看设置,number_of_routing_shards设置不可见..
GET split_test_index

# 插入测试数据
PUT split_test_index/_doc/1
{
  "name":"zhangsan"
}
PUT split_test_index/_doc/2
{
  "name":"lisi"
}
# 设为只读
PUT /split_test_index/_settings
{
  "settings": {
    "index.blocks.write": true 
  }
}
# 切分索引
POST split_test_index/_split/split_test_index_new?copy_settings=true
{
  "settings": {
    "index.number_of_shards": 2,
    "index.number_of_replicas": 2
  }
}
# 查看切分后索引分片数
GET split_test_index/_settings
# 查看分片信息
GET _cat/shards/split_test_index_new
# 查看索引数据
GET split_test_index_new/_search
# 切分完成后需要去除只读设置(原索引/切分后索引都需要)
PUT /split_test_index/_settings
{
  "settings": {
    "index.blocks.write": null 
  }
}
```



* 添加mapping
```markdown
# 只允许添加字段,或将已有字段改为只能查询
PUT twitter/_mapping/_doc 
{
  "properties": {
    "email": {
      "type": "keyword"
    }
  }
}
# 更新两个索引(可以使用通配符)的mapping,
PUT /twitter-1,twitter-2/_mapping/_doc 
{
  "properties": {
    "user_name": {
      "type": "text"
    }
  }
}
```
* 查看mapping
```markdown
# 查看twitter索引的mapping
GET /twitter/_mapping
# 支持通配符
GET /_all/_mapping
# 查看单个字段的mapping
GET /twitter/_mapping/field/field1
# 支持通配符(不支持_all),多字段逗号连接
GET /twitter/_mapping/field/f*
```
* 别名
```markdown
# 添加别名
POST /_aliases
{
    "actions" : [
        { "add" : { "index" : "twitter", "alias" : "aliase_twitter" } }
    ]
}
#  删除别名
POST /_aliases
{
    "actions" : [
        { "remove" : { "index" : "twitter", "alias" : "aliase_twitter" } }
    ]
}
# 重命名,先删除,后新增
POST /_aliases
{
    "actions" : [
        { "remove" : { "index" : "twitter", "alias" : "aliase_twitter" } },
        { "add" : { "index" : "twitter", "alias" : "aliase_twitter_rname" } }
    ]
}
# 查询验证
GET aliase_twitter/_search
GET aliase_twitter_rname/_search
GET twitter/_search
```
* 修改索引分词器
```markdown
# 修改设置 https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-update-settings.html#update-settings-analysis
# 分析器 https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html#_specifying_an_index_time_analyzer
# 自定义分析器 https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-custom-analyzer.html

# 删除索引
DELETE twitter
# 创建索引,不指定分词器(使用es默认的)
PUT twitter
{
  "settings" : {
    "number_of_shards" : 4,   
    "number_of_replicas" : 1 
  },
  "mappings" : {
    "_doc" : {
      "properties" : {
        "content" : { "type" : "text"  }
      }
    }
  }
}
# 查看索引
GET twitter
# 放文档
PUT twitter/_doc/4
{"content":"潍柴位于山东潍坊市"}
# 查询文档
GET twitter/_search
# 高亮检索测试
GET twitter/_search
{
  "query":{ "match" : { "content" : "潍坊" }},
  "highlight" : {
        "pre_tags" : ["<tag1>", "<tag2>"],
        "post_tags" : ["</tag1>", "</tag2>"],
        "fields" : {
            "content" : {}
        }
    }
}
# 更改分析器(需关闭索引)
POST /twitter/_close
PUT /twitter/_settings
{
  "analysis" : {
    "analyzer":{
      "default":{
        "type":      "custom", 
        "tokenizer": "ik_max_word"
      },
      "default_search":{
        "type":      "custom", 
        "tokenizer": "ik_max_word"
      }
    }
  }
}
POST /twitter/_open
# 执行文档更新
POST twitter/_update_by_query?refresh&conflicts=proceed
# 查询文档
GET twitter/_search
# 高亮检索测试
GET twitter/_search
{
  "query":{ "match" : { "content" : "潍坊" }},
  "highlight" : {
        "pre_tags" : ["<tag1>", "<tag2>"],
        "post_tags" : ["</tag1>", "</tag2>"],
        "fields" : {
            "content" : {}
        }
    }
}
# 分析
POST twitter/_analyze
{
  "analyzer": "default",
  "text": "潍柴位于山东潍坊市"
}
```
* 索引设置
```markdown
# 修改副本数
PUT /twitter/_settings
{
    "index" : {
        "number_of_replicas" : 2
    }
}
# 修改刷新时间
PUT /twitter/_settings
{
    "index" : {
        "refresh_interval" : "1s"
    }
}
# 查看索引配置
GET /twitter/_settings
# 通配符支持
GET /twitter/_settings/*num
```
* 索引状态
```markdown
# 全部属性
GET twitter/_stats
# 文档数
GET twitter/_stats/docs
# docs  文档数信息
# store 索引大小
# indexing 索引(数据插入)信息统计(即插入了多少次数据,耗时信息)
# search 检索信息统计(查询次数,查询耗时信息)
# segments 段信息
```
* 段信息
```markdown
# 查看段信息
GET /twitter/_segments
# _0 段名称
# generation 代数,插入一个文档(插入到当前段)+1
# num_docs 未删除的文档数
# deleted_docs 已删除的文档数
# size_in_bytes 段大小(磁盘)
# memory_in_bytes 段大小(内存)
# committed 是否已刷到磁盘
# search 是否可查询
# compound 是否存在一个复合文件中..
# attributes 段存储规则(性能优先,压缩优先)

# 段刷新到磁盘,默认是基于内存启发(memory heuristics),自动刷盘
POST twitter/_flush
# 段合并
POST /twitter/_forcemerge

```
