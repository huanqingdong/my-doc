## [Cluster Health](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/cluster-health.html)

> 集群健康有绿,黄,红,三个状态, 红色表明有主分片未正常分配,黄色表示主分片已分配,单复制分片没有分配.绿色表示所有分片已分配

```nginx
# 查看集群健康
GET _cluster/health
# 可以针对索引进行查询,支持(*和,)
GET _cluster/health/logstash*


```

