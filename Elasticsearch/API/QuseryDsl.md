## [Query and filter context](https://www.elastic.co/guide/en/elasticsearch/reference/7.0/query-filter-context.html)

* Query context

  > *How well does this document match this query clause?*
  >
  > 不仅仅是yes/no,会影响评分

* Filter context

  > *Does this document match this query clause?*
  >
  > 仅筛选,不影响得分

## [Match All Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-all-query.html)

```shell
# 查询全部
GET twitter/_doc/_search
{"query": {
        "match_all": {"boost" : 1.2}
    }
}
# 全不查询
GET twitter/_doc/_search
{"query": {
        "match_none": {"boost" : 1.2}
    }
}
```

## [Full text queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/full-text-queries.html)

### [Match Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html)

> `operator` : and |or 指定个关键词检索结果间的关系
>
> [`minimum_should_match`](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-minimum-should-match.html) : 最小匹配个数
>
> `analyzer`:用于解析查询文本的分词器,默认为字段上指定的分词器,或者default search analyzer
>
> `lenient` :用户忽略字段类型不匹配,默认false
>
> `fuzziness` :用户忽略英文单词中的错误字符数,中文好像作用不大

```nginx
# match and or 测试
#  创建测试用索引
PUT twitter
{
  "settings" : {
    "number_of_shards" : 4,   
    "number_of_replicas" : 1 
  },
  "mappings" : {
    "_doc" : {
      "properties" : {
        "content" : { 
          "type" : "text" ,
          "analyzer":"ik_max_word"
        }
      }
    }
  }
}
# 增加2个文档
PUT twitter/_doc/4
{"content":"潍柴位于山东潍坊市"}
PUT twitter/_doc/2
{"content":"潍柴位于潍坊市"}
# and or 测试
GET twitter/_search
{
    "query": {
        "match" : {
            "content" :{
                "query":"潍坊+山东",
                "operator" : "or"
            } 
        }
    }
}
# 指定返回字段
GET /_search
{
    "_source": {
        "includes": [ "obj1.*", "obj2.*" ],
        "excludes": [ "*.description" ]
    },
    "query" : {
        "term" : { "user" : "kimchy" }
    }
}
```

### [Match Phrase Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query-phrase.html)

```nginx
# 短语匹配
PUT test
{
  "settings": {
    "analysis" : {
    "analyzer":{
      "default":{
        "type":      "custom", 
        "tokenizer": "ik_max_word"
      },
      "default_search":{
        "type":      "custom", 
        "tokenizer": "ik_max_word"
      }
    }
  }
  }, 
  "mappings": {
    "_doc":{
      "properties" : {
        "msg" : { "type" : "text"
        }
      }
    }
  }
}
# 分析
GET test/_analyze
{
  "field": "msg",
  "text": ["潍柴动位于潍坊市"]
  
}

PUT test/_doc/1
{
  "msg":"潍柴动力位于潍坊市"
}
PUT test/_doc/2
{
  "msg":"潍柴动位于潍坊市"
}
PUT test/_doc/3
{
  "msg":"潍柴动力就位于潍坊市"
}
PUT test/_doc/4
{
  "msg":"潍坊位于潍柴动力"
}
# slop允许乱序
GET test/_search
{
    "query": {
        "match_phrase" : {
            "msg" : {
              "query": "潍柴动力位于",
              "slop": 4
            }
            
        }
    }
}

```

### [Match Phrase Prefix Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query-phrase-prefix.html)

> Consider the query string quick brown f. This query works by creating a phrase query out of quick and brown (i.e. the term quick must exist and must be followed by the term brown). Then it looks at the sorted term dictionary to find the first 50 terms that begin with f, and adds these terms to the phrase query.

``` nginx
# max_expansions:个数, 取多少个以最后词扩展的词查询

GET test/_search
{
    "query": {
        "match_phrase_prefix" : {
            "msg" : {
              "query": "潍柴动",
              "max_expansions" : 2
            }
        }
    }
}
```

### [Multi Match Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html)

```nginx
# 索引,数据准备
PUT test
{
  "settings": {
    "analysis" : {
    "analyzer":{
      "default":{
        "type":      "custom", 
        "tokenizer": "ik_max_word"
      },
      "default_search":{
        "type":      "custom", 
        "tokenizer": "ik_max_word"
      }
    }
  }
  }, 
  "mappings": {
    "_doc":{
      "properties" : {
        "msg" : { "type" : "text"},
        "title" : { "type" : "text"}
      }
    }
  }
}
# 数据准备
PUT test/_doc/1
{
  "msg":"消息主体",
  "title":"潍柴动力"
}
PUT test/_doc/2
{
  "msg":"潍柴动力",
  "title":"消息标题"
}
PUT test/_doc/3
{
  "msg":"潍柴动力",
  "title":"潍柴动力"
}
# 分词分析
GET test/_analyze
{
  "field": "msg",
  "text": ["潍柴动力","消息主体","消息标题"]
}

# 多字段检索
GET test/_search
{
  "query": {
    "multi_match" : {
      "query":    "潍柴", 
      "fields": [ "msg", "title" ] 
    }
  }
}
# fields可以使用通配符
GET test/_search
{
  "query": {
    "multi_match" : {
      "query":    "潍柴", 
      "fields": [ "msg", "*tle" ] 
    }
  }
}
# 可以通过(^)提升字段的分值比重,以下title是msg字段三倍比重
GET test/_search
{
  "query": {
    "multi_match" : {
      "query":    "潍柴", 
      "fields": [ "msg", "title^3" ] 
    }
  }
}
# 如果fields不指定值,则使用index.query.default_field index settings,默认*.*,所有合适的字段
# 查询字段数受indices.query.bool.max_clause_count限制,默认1024个
```

#### best_fields

>   最高分+(其余字段得分)*tie_breaker, tie_breaker默认值为0 

```nginx
# 不加参数tie_breaker,得分一样;加上参数后,msg,title中都包含潍柴的doc3分值最高
GET test/_search
{
  "query": {
    "multi_match" : {
      "query":    "潍柴", 
      "fields": [ "msg", "title" ] ,
      "tie_breaker": 0.3
    }
  }
}
```

> `best_fields` 和`most_fields` 类型为每个字段生成一个查询. 这意味着 `operator` and `minimum_should_match` parameters 被应用到每个字段,示例
>
> ```nginx
> GET /_search
> {
>   "query": {
>     "multi_match" : {
>       "query":      "Will Smith",
>       "type":       "best_fields",
>       "fields":     [ "first_name", "last_name" ],
>       "operator":   "and" 
>     }
>   }
> }
> # This query is executed as:
>  (+first_name:will +first_name:smith)
> | (+last_name:will  +last_name:smith)
> # In other words, all terms must be present in a single field for a document to match.
> ```

#### most_fields

> 分数是各字段分求和

#### phraseand phrase_prefix

The `phrase` and `phrase_prefix` types behave just like [`best_fields`](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html#type-best-fields), but they use a `match_phrase` or `match_phrase_prefix` query instead of a `match` query.

####  cross_fields

> https://www.elastic.co/guide/cn/elasticsearch/guide/current/_cross_fields_queries.html
>
> `cross_fields` 使用词中心式（term-centric）的查询方式，这与 `best_fields` 和 `most_fields` 使用字段中心式（field-centric）的查询方式非常不同，它将所有字段当成一个大字段，并在 *每个字段* 中查找 *每个词* 。

### [Common Terms Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-common-terms-query.html)

> common terms查询是stopwords的现代替代品，它在不牺牲性能的前提下(通过考虑stopwords)提高了搜索结果的精确度和召回率

## [Term level queries](https://www.elastic.co/guide/en/elasticsearch/reference/current/term-level-queries.html)

### [Term Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-term-query.html)

```shell
# 创建索引,
# The full_text field is of type text and will be analyzed.
# The exact_value field is of type keyword and will NOT be analyzed.
PUT my_index
{
  "mappings": {
    "_doc":{
      "properties": {
        "full_text": {
          "type":  "text" 
        },
        "exact_value": {
          "type":  "keyword" 
        }
      }
    }
  }
}
# 插入数据
# The full_text inverted index will contain the terms: [quick, foxes].
# The exact_value inverted index will contain the exact term: [Quick Foxes!].
PUT my_index/_doc/1
{
  "full_text":   "Quick Foxes!", 
  "exact_value": "Quick Foxes!"  
}
# This query matches because the exact_value field contains the exact term Quick Foxes!.
GET my_index/_search
{
  "query": {
    "term": {
      "exact_value": "Quick Foxes!" 
    }
  }
}
# This query does not match, because the full_text field only contains the terms quick and foxes. It do有
GET my_index/_search
{
  "query": {
    "term": {
      "full_text": "Quick Foxes!" 
    }
  }
}
# A term query for the term foxes matches the full_text field.
GET my_index/_search
{
  "query": {
    "term": {
      "full_text": "foxes" 
    }
  }
}
# This match query on the full_text field first analyzes the query string, then looks for documents containing quick or foxes or both.
GET my_index/_search
{
  "query": {
    "match": {
      "full_text": "Quick Foxes!" 
    }
  }
}
```

### [Terms Query](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-terms-query.html)

> 被检索索引(users),如果数据量不大,可以使用单分片,每个节点一个副本的形式来存储,这样在查询terms的时候就不会进行跨节点传输,节省网络开销.
>
> terms值默认限定在65536,可以通过index.max_terms_count来更改
>
> 大量terms查询会很慢 Executing a Terms Query request with a lot of terms can be quite slow, as each additional term demands extra processing and memory.

```shell
# 构建tweets用户数据,u01,u02,u03
PUT /tweets/_doc/1
{
    "user" : "u01"
}
PUT /tweets/_doc/2
{
    "user" : "u02"
}
PUT /tweets/_doc/3
{
    "user" : "u03"
}
# 插入term值查询数据

PUT /users/_doc/1
{
    "followers" : ["u01", "u03"]
}
# 查询
GET /tweets/_search
{
    "query" : {
        "terms" : {
            "user" : {
                "type":"_doc",
                "index" : "users",
                "id" : "1",
                "path" : "followers"
            }
        }
    }
}
# 对象字段形式
DELETE users
PUT /users/_doc/2
{
 "followers" : [
   {
     "id" : "u01"
   },
   {
     "id" : "u02"
   }
 ]
}
GET /tweets/_search
{
    "query" : {
        "terms" : {
            "user" : {
                "type":"_doc",
                "index" : "users",
                "id" : "2",
                "path" : "followers.id"
            }
        }
    }
}
```

### [Terms Set Query](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/query-dsl-terms-set-query.html)

> 可通过字段或者脚本控制最小匹配数量
>
> The number of terms that must match varies per document and is either controlled by a minimum should match field or computed per document in a minimum should match script.

```nginx
# 数据构建
PUT /my-index
{
    "mappings": {
        "_doc": {
            "properties": {
                "required_matches": {
                    "type": "long"
                }
            }
        }
    }
}
PUT /my-index/_doc/1?refresh
{
    "codes": ["ghi", "jkl"],
    "required_matches": 2
}
PUT /my-index/_doc/2?refresh
{
    "codes": ["def", "ghi"],
    "required_matches": 2
}
# 通过字段指定最小匹配值
GET /my-index/_search
{
    "query": {
        "terms_set": {
            "codes" : {
                "terms" : ["abc", "def", "ghi"],
                "minimum_should_match_field": "required_matches"
            }
        }
    }
}
# 通过脚本指定
GET /my-index/_search
{
    "query": {
        "terms_set": {
            "codes" : {
                "terms" : ["abc", "def", "ghi"],
                "minimum_should_match_script": {
                   "source": "Math.min(params.num_terms, doc['required_matches'].value)"
                }
            }
        }
    }
}
# 不支持通过minimum_should_match指定固定值,可以使用脚本实现
GET /my-index/_search
{
    "query": {
        "terms_set": {
            "codes" : {
                "terms" : ["abc", "def", "ghi"],
                "minimum_should_match_script": {
                   "source": "2"
                }
            }
        }
    }
}
```

### [Range Query](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/query-dsl-range-query.html)

| 符号  | 含义                     |
| ----- | ------------------------ |
| `gte` | Greater-than or equal to |
| `gt`  | Greater-than             |
| `lte` | Less-than or equal to    |
| `lt`  | Less-than                |

Date类型比较精确到秒,也就是毫秒的值不一样会认为是相等的(1420070400000 = 1420070400001)

```shell
DELETE my_index
PUT my_index
{
  "mappings": {
    "_doc": {
      "properties": {
        "date": {
          "type": "date" ,
          "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
        }
      }
    }
  }
}

PUT my_index/_doc/1
{ "date": "2015-01-01" } 

PUT my_index/_doc/2
{ "date": "2015-01-01 12:10:30" } 

PUT my_index/_doc/3
{ "date": 1420070400001 } 

GET my_index/_search
{
  "sort": { "date": "asc"} 
}


GET my_index/_search
{
    "query": {
        "range" : {
            "date" : {
                "gte": "2015-01-01 00:00:00", 
                "lte": "now"
            }
        }
    }
}
```

### [Exists Query](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/query-dsl-exists-query.html)

> Returns documents that have at least one non-`null` value in the original field:

```nginx
GET /_search
{
    "query": {
        "exists" : { "field" : "user" }
    }
}
For instance, these documents would all match the above query:

{ "user": "jane" }
# An empty string is a non-null value.
{ "user": "" } 
# Even though the standard analyzer would emit zero tokens, the original field is non-null.
{ "user": "-" } 
{ "user": ["jane"] }
# At least one non-null value is required.
{ "user": ["jane", null ] } 

These documents would not match the above query:

{ "user": null }
# This field has no values.
{ "user": [] } 
# At least one non-null value is required.
{ "user": [null] } 
# The user field is missing completely.
{ "foo":  "bar" } 
# 可以通过null_value属性,在插入时将null值转化为别的内容,以下会将null转为"_null_"字符串
PUT /example
{
  "mappings": {
    "_doc": {
      "properties": {
        "user": {
          "type": "keyword",
          "null_value": "_null_"
        }
      }
    }
  }
}
```

### [Prefix Query](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/query-dsl-prefix-query.html)

```nginx
# 索引创建
PUT test_prifix
{
  "mappings": {
    "_doc":{
      "properties":{
        "title":{
          "type":"keyword"
        },
        "msg":{
          "type":"text",
          "analyzer":"ik_max_word"
        }
      }
    }
  }
}
# 数据准备, title:["潍柴动力"] ,msg:["潍柴","动力","潍柴动力"]
PUT test_prifix/_doc/1
{
  "title":"潍柴动力",
  "msg":"潍柴动力"
}
# 通过潍前缀检索,title|msg都能检索到
GET test_prifix/_search
{ "query": {
    "prefix" : { "title": "潍" }
  }
}
GET test_prifix/_search
{ "query": {
    "prefix" : { "msg": "潍" }
  }
}
# 通过动前缀检索,只有msg能检索到
GET test_prifix/_search
{ "query": {
    "prefix" : { "title": "动" }
  }
}
GET test_prifix/_search
{ "query": {
    "prefix" : { "msg": "动" }
  }
}
# 加权重
GET test_prifix/_search
{ "query": {
    "prefix" : { "msg" :  { "value" : "潍", "boost" : 2.0 } }
  }
}
```

### [Wildcard Query](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/query-dsl-wildcard-query.html)

```nginx
DELETE test_wildcard
PUT test_wildcard
{
  "mappings": {
    "_doc":{
      "properties":{
        "title":{
          "type":"keyword"
        },
        "msg":{
          "type":"text",
          "analyzer":"ik_max_word"
        }
      }
    }
  }
}
# 数据准备, title:["潍柴动力"] ,msg:["潍柴","动力","潍柴动力"]
PUT test_wildcard/_doc/1
{
  "title":"潍柴动力",
  "msg":"潍柴动力"
}
# title字段使用[潍*,潍???]能匹配到
GET test_wildcard/_search
{ "query": {
    "wildcard" : { "title": "潍*" }
  }
}
# msg字段使用[潍*,潍?,潍???]能匹配到
GET test_wildcard/_search
{ "query": {
    "wildcard" : { "msg": "潍???" }
  }
}
# 加权重
GET test_wildcard/_search
{
    "query": {
        "wildcard" : { "msg" : { "value" : "潍*", "boost" : 1.0 } }
    }
}

```

### [Regexp Query](https://www.elastic.co/guide/en/elasticsearch/reference/6.6/query-dsl-regexp-query.html)

```nginx

```

