## [Aggregations](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations.html)

```shell
# 结构
"aggregations" : {
    "<aggregation_name>" : {
        "<aggregation_type>" : {
            <aggregation_body>
        }
        [,"meta" : {  [<meta_data_body>] } ]?
        [,"aggregations" : { [<sub_aggregation>]+ } ]?
    }
    [,"<aggregation_name_2>" : { ... } ]*
}
```

### [Metrics Aggregations](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics.html)

#### [Avg Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-avg-aggregation.html)

```shell
# 统计平均值,size=0表示不查询明细文档
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "avg_took" : { "avg" : { "field" : "took" } }
  }
}
# 脚本,对took*10
POST /es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "avg_took" : { 
      "avg" : { 
        "field": "took", 
        "script": {
          "lang": "painless", 
          "source" : "_value * params.correction", 
          "params": {
            "correction" : 10
          }
        }
      } 
    }
  }
}
```

#### [Weighted Avg Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-weight-avg-aggregation.html)

```shell
# 计算公式 ∑(value * weight) / ∑(weight)
POST /es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "avg_took_weighted" : { 
      "weighted_avg":{
       "value": {
          "field": "took"
        },
        "weight": {
          "field": "searchParams.pageSize"
        }
      }
    }
  }
}
```

#### [Cardinality Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-cardinality-aggregation.html)

```shell
# 此值是不保证准确的
# 计算某个字段的(distinct count),当字段为text时,计算的是分词后的数
POST /es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "keyword_count" : { 
      "cardinality": {
        "field": "searchParams.keyword.keyword"
      }
    }
  }
}
```

#### [Extended Stats Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-extendedstats-aggregation.html)

```shell
# 统计信息
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_stat" : { 
      "extended_stats": { 
        "field" : "took" 
      } 
    }
  }
}
# 结果
"aggregations" : {
    "took_stat" : {
      "count" : 106, 
      "min" : 3.0,
      "max" : 1499.0,
      "avg" : 520.3679245283018,
      "sum" : 55159.0,
      "sum_of_squares" : 3.5063741E7,# 平方和
      "variance" : 60007.232556069765,# 方差
      "std_deviation" : 244.963737226696,# 标准差
      "std_deviation_bounds" : {
        "upper" : 1010.2953989816938,
        "lower" : 30.440450074909847
      }
    }
  }

```

#### [Max Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-max-aggregation.html)

```shell
# 最大值
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_max" : { 
      "max": { 
        "field" : "took" 
      } 
    }
  }
}
```

#### [Min Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-min-aggregation.html)

```shell
# 最小值
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_min" : { 
      "min": { 
        "field" : "took" 
      } 
    }
  }
}
```

#### [Percentiles Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-percentile-aggregation.html)

```shell
# 统计各百分位上的值,如下分别统计95%,99%,99.9%的最大耗时
# percents自定义百分比列表,默认[ 1, 5, 25, 50, 75, 95, 99 ]
# keyed是否以kv方式显示查询结果,默认true.
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_outlier" : { 
      "percentiles": { 
        "field" : "took" ,
        "percents" : [95, 99, 99.9],
        "keyed":false
      } 
    }
  }
}
```

#### [Percentile Ranks Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-percentile-rank-aggregation.html)

```shell
# 统计某个值之下的百分比,如下分别统计took小于500,600,1000的百分比
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_rank" : { 
      "percentile_ranks": { 
        "field" : "took" ,
        "values" : [500, 600,1000],
        "keyed":false
      } 
    }
  }
}
```

#### [Scripted Metric Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-scripted-metric-aggregation.html)

```shell
# 计算个数
POST es_search_log-2019.05.24/_search?size=0
{
  "query" : {
      "match_all" : {}
  },
  "aggs": {
    "profit": {
    "scripted_metric": {
      "init_script" : "state.transactions = []", 
      "map_script" : "state.transactions.add(doc.took.value > 1000 ? 1 : 0)",
      "combine_script" : "double profit = 0; for (t in state.transactions) { profit += t } return profit",
      "reduce_script" : "double profit = 0; for (a in states) { profit += a+1 } return profit"
    }
    }
  }
}
```

#### [Top Hits Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-metrics-top-hits-aggregation.html)

```shell
# 检索最高的关键字,带明细
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs": {
    "top_tags": {
      "terms": {
        "field": "searchParams.keyword.keyword",
        "size": 2
      },
      "aggs": {
        "top_sales_hits": {
          "top_hits": {
            "sort": [
              {
                "took": {
                  "order": "desc"
                }
              }
            ],
            "_source": {
              "includes": "searchParams.*"
            },
            "size" : 1
          }
        }
      }
    }
  }
}
```

#### [Value Count Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-metrics-valuecount-aggregation.html)

```shell
# 计算数据条数
POST /es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "keyword_count" : { 
      "value_count": {
        "field": "searchParams.keyword.keyword"
      }
    }
  }
}
```

### [Bucket Aggregations](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket.html)

一个请求返回的最大桶数被动态集群级设置search.max_buckets限制,默认值10000

#### [Auto-interval Date Histogram Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-autodatehistogram-aggregation.html)

```shell
# 自动时间分段直方图统计,如下分10段统计访问量
POST /es_search_log-*/_search?size=0
{
  "aggs" : {
      "search_over_time" : {
        "auto_date_histogram" : {
          "field" : "@timestamp",
          "buckets" : 10,
          "format" : "yyyy-MM-dd",
          "time_zone": "+08:00"
        }
      }
  }
}

```

#### [Composite Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-composite-aggregation.html)

```shell
# sources terms 
GET es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "my_buckets": {
      "composite" : {
          "sources" : [
            { "keyword": { "terms" : { "field": "searchParams.keyword.keyword" } } }
          ]
      }
    }
   }
}
# soruces histogram 
GET es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "my_buckets": {
      "composite" : {
          "sources" : [
            { "took": { "histogram": { 
              "field": "took", "interval": 200 } } }
          ]
      }
    }
   }
}
# soruces date_histogram,时区不管用
GET es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "my_buckets": {
      "composite" : {
        "sources" : [
          { "date": { "date_histogram": { 
            "field": "@timestamp",
            "interval": "10m",
            "time_zone": "+08:00",
            "format": "yyyy-MM-dd hh:mm Z"
          } } }
        ]
      }
    }
  }
}
# 使用after进行翻页
GET es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "my_buckets": {
      "composite" : {
          "sources" : [
            { "keyword": { "terms" : { "field": "searchParams.keyword.keyword" } } }
          ],
          "after" : {
            "keyword" : "技术 2"
          }
      }
    }
   }
}
```

#### [Date Histogram Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-datehistogram-aggregation.html)

```shell
# 按时间范围统计文档数
POST /es_search_log-*/_search?size=0
{
  "aggs" : {
      "search_over_time" : {
        "date_histogram" : {
          "field" : "@timestamp",
          "interval": "2h",
          "time_zone": "+08:00"
        }
      }
  }
}
```

#### [Date Range Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-daterange-aggregation.html)

```shell
# 计算时间小于to的记录数,计算时间大于from的记录数
POST /es_search_log-*/_search?size=0
{
  "aggs" : {
      "range" : {
        "date_range": {
          "field" : "@timestamp",
          "time_zone": "+08:00",
          "ranges": [
            {"to": "now-2d/d"},
             {"from": "now-10d/d"}
          ]
        }
      }
  }
}
```

#### [Filter Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-filter-aggregation.html)

```shell
# 进行筛选后再聚合
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_filter" : { 
      "filter": { 
        "term": {
          "searchParams.keyword.keyword": "中国"
        }
      },
      "aggs" : {
        "avg_took" : { "avg" : { "field" : "took" } },
        "sum_took" : { "sum" : { "field" : "took" } }
      }
    }
  }
}
```

#### [Filters Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-filters-aggregation.html)

```shell
# 统计各级别日志数量
GET logstash-*/_search
{
  "size": 0,
  "aggs" : {
    "messages" : {
      "filters" : {
        "other_bucket_key": "other_messages",
        "filters" : {
          "ERROR" :   { "match" : { "level" : "ERROR"}},
          "WARN" : { "match" : { "level" : "WARN" }},
          "INFO" : { "match" : { "level" : "INFO" }},
          "DEBUG" : { "match" : { "level" : "DEBUG" }}
        }
      }
    }
  }
}
```

#### [Global Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-global-aggregation.html)

```shell
POST /es_search_log-2019.05.24/_search?size=0
{
  "query" : {
    "term": {
      "searchParams.keyword.keyword": "中国"
    }
  },
  "aggs" : {
    "all_products" : {
        "global" : {}, 
        "aggs" : { 
          "avg_took_g" : { "avg" : { "field" : "took" } },
          "sum_took_g" : { "sum" : { "field" : "took" } }
        }
    },
    "avg_took_ch": { "avg" : { "field" : "took" } },
    "sum_took_ch": { "sum" : { "field" : "took" } }
  }
}

```

#### [Histogram Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-histogram-aggregation.html)

```shell
# bucket_key = Math.floor((value - offset) / interval) * interval + offset
# value如果小于offset,key会出现负值
# min_doc_count能筛选掉小于该值得聚合结果
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_h" : { 
      "histogram": { 
        "field": "took",
        "interval": 200,
        "min_doc_count" : 1
      }
    }
  }
}
# extended_bounds,当范围内无数据时,依然生成KEY
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_h" : { 
      "histogram": { 
        "field": "took",
        "interval": 200,
        "extended_bounds": {
          "min": 400,
          "max": 3000
        }
      }
    }
  }
}
# min_doc_count和extended_bounds同时定义时,extended_bounds无意义
POST /es_search_log-2019.05.24/_search?size=0
{
  "aggs" : {
    "took_h" : { 
      "histogram": { 
        "field": "took",
        "interval": 200,
        "min_doc_count" : 1,
        "extended_bounds": {
          "min": 400,
          "max": 1000
        }
      }
    }
  }
}

```

#### [Missing Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-missing-aggregation.html)

```shell
# 统计fullType字段无值的记录数
POST /pdm_doc/_search?size=0
{
  "aggs" : {
    "without_fullType" : {
      "missing" : { "field" : "fullType.keyword" }
    }
  }
}

```

#### [Range Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-range-aggregation.html)

```shell
# 范围统计,如下分别统计took小于500,在500~1000之间,大于1000的数量
GET es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "took_ranges" : {
      "range" : {
        "field" : "took",
        "keyed":true,
        "ranges" : [
          { "to" : 500.0 },
          { "from" : 500.0, "to" : 1000.0 },
          { "from" : 1000.0 }
        ]
      }
    }
  }
}
# 带子统计
GET es_search_log-2019.05.24/_search
{
  "size": 0, 
  "aggs" : {
    "took_ranges" : {
      "range" : {
        "field" : "took",
        "keyed":true,
        "ranges" : [
          { "to" : 500.0 },
          { "from" : 500.0, "to" : 1000.0 },
          { "from" : 1000.0 }
        ]
      },
      "aggs" : {
        "took_stats" : {
          "stats" : { "field" : "took" }
        }
      }
    }
  }
}
```

#### [Significant Text Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-bucket-significanttext-aggregation.html)

```shell
# 
GET pdm_doc/_search
{
  "size": 0, 
    "query" : {
        "match": {
          "content": "技术"
        }
    },
    "aggregations" : {
        "significant_crime_types" : {
            "significant_text" : { "field" : "content" }
        }
    }
}
# 使用采样器
GET pdm_doc/_search
{
  "size": 0, 
  "query" : {
      "match": {
        "content": "潍柴"
      }
  },
  "aggs" : {
    "mysampler":{
       "sampler":{
        "shard_size": 100
      },
      "aggs":{
        "significant_crime_types" : {
          "significant_text" : { "field" : "content" }
        }
      }
    }
  }
}
```

### [Pipeline Aggregations](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-pipeline.html)

#### [Avg Bucket Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-pipeline-avg-bucket-aggregation.html)

```shell
# 对聚合后的数据求平均,此处对每天的耗时求平均
POST es_search_log-2019.05.24,es_search_log-2019.05.25/_search
{
  "size": 0,
  "aggs": {
    "tooks_per_day": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "day"
      },
      "aggs": {
        "took_sum": {
          "sum": {
            "field": "took"
          }
        }
      }
    },
    "avg_daily_tooks": {
      "avg_bucket": {
        "buckets_path": "tooks_per_day>took_sum" 
      }
    }
  }
}
```

#### [Derivative Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-pipeline-derivative-aggregation.html)

```shell
# 导数, 求直方图中下一个值较上一个值的变动数
GET logstash-*/_search
{
  "size": 0,
  "aggs" : {
    "logs_per_day" : {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "day"
      },
      "aggs": {
        "logs": {
          "value_count": {
            "field": "_id"
          }
        },
        "logs_derv":{
          "derivative": {
            "buckets_path": "logs"
          }
        }
      }
    }
  }
}

```

#### [Bucket Selector Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-pipeline-bucket-selector-aggregation.html)

```shell
# 筛选bucket
POST es_search_log-2019.05.24,es_search_log-2019.05.25/_search
{
  "size": 0,
  "aggs": {
    "tooks_per_day": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "day"
      },
      "aggs": {
        "took_sum": {
          "sum": {
            "field": "took"
          }
        },
        "took_bucket_sort": {
          "bucket_sort": {
            "sort": [
              {"took_sum": {"order": "asc"}}
            ],
            "size": 3
          }
        },
        "sales_bucket_filter": {
          "bucket_selector": {
            "buckets_path": {
              "tookSum": "took_sum"
            },
            "script": "params.tookSum > 20000"
          }
        }
      }
    },
    "avg_daily_tooks": {
      "avg_bucket": {
        "buckets_path": "tooks_per_day>took_sum" 
      }
    }
  }
}
```



#### [Bucket Sort Aggregation](https://www.elastic.co/guide/en/elasticsearch/reference/7.1/search-aggregations-pipeline-bucket-sort-aggregation.html)

```shell
# 排序bucket
POST es_search_log-2019.05.24,es_search_log-2019.05.25/_search
{
  "size": 0,
  "aggs": {
    "tooks_per_day": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "day"
      },
      "aggs": {
        "took_sum": {
          "sum": {
            "field": "took"
          }
        },
        "took_bucket_sort": {
          "bucket_sort": {
            "sort": [
              {"took_sum": {"order": "asc"}}
            ],
            "size": 3
          }
        }
      }
    },
    "avg_daily_tooks": {
      "avg_bucket": {
        "buckets_path": "tooks_per_day>took_sum" 
      }
    }
  }
}
```

