* [Index Api](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html)
```markdown
# 添加或更新文档 
# PUT indexName/docType/docId
PUT twitter/_doc/1
{
    "user" : "kimchy",
    "post_date" : "2009-11-15T14:12:12",
    "message" : "trying out Elasticsearch"
}
```
* [Get Api](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-get.html)
```markdown
# 查询文档
# GET indexName/docType/docId
GET twitter/_doc/1
# 判断文档是否存在
# HEAD indexName/docType/docId
HEAD twitter/_doc/1
# 不显示_source
GET twitter/_doc/1?_source=false
# 指定包含字段+排除字段 
GET twitter/_doc/1?_source_include=*.id&_source_exclude=entities
# 仅指定包含字段
GET twitter/_doc/1?_source=*.id,retweeted
# 仅检索_soruce
GET twitter/_doc/1/_source
GET twitter/_doc/1/_source?_source=*.id,retweeted
```
* [Delete Api](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-delete.html)
```markdown
# 删除文档 
# DELETE indexName/docType/docId
DELETE /twitter/_doc/1
```
* [Delete by query Api](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-delete-by-query.html)
```markdown
# 通过查询进行删除
POST twitter/_delete_by_query
{
  "query": { 
    "match": {
      "user" : "kimchy"
    }
  }
}
# 删除全部数据
POST pdm_doc_v5/_delete_by_query
{
  "query":{
    "match_all": {}
  }
}

```
* [Update Api](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-update.html)
```markdown
# 放一个测试文档
PUT test/_doc/1
  {
      "counter" : 1,
      "tags" : ["red"]
  }
 #  脚本更新 ,对counter字段更新
 POST test/_doc/1/_update
 {
     "script" : {
         "source": "ctx._source.counter += params.count",
         "lang": "painless",
         "params" : {
             "count" : 4
         }
     }
 }
 # 文档部分更新
 POST test/_doc/1/_update
 {
     "doc" : {
         "name" : "new_name"
     }
 }
 
 # upsert
 POST test/_doc/1/_update
 {
     "doc" : {
         "name" : "new_name"
     },
     "doc_as_upsert" : true
 }
```
* Update by query Api
```markdown
# 更新全部文档
POST twitter/_update_by_query?conflicts=proceed
```

