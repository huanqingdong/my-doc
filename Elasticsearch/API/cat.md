## [cat aliases](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-alias.html)

```shell
# 查看全部别名
GET /_cat/aliases?v
# 使用模糊匹配查询
GET /_cat/aliases/logstash*?v
# 查询多个,中间只用,分割
GET /_cat/aliases/alias1,alias2?v
```

## [cat allocation](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-allocation.html)

```shell
# 查看各节点有多少分片及占用的磁盘信息
GET /_cat/allocation?v
# 模糊查询
GET /_cat/allocation/node-*?v
# 匹配多个
GET /_cat/allocation/node-223,node-224?v
```

## [cat count](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-count.html)

```shell
# 查看集群中文档数量
GET /_cat/count?v
# 查看单个索引文档数量(索引名称支持模糊*和多个,)
GET /_cat/count/pdm_doc?v
```

## [cat fielddata](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-fielddata.html)

```shell
# 查看fielddata占用的java heap空间
GET /_cat/fielddata?v
# fileds能够作为查询参数或者路径参数(执行*和,),如下
# 参数方式
GET /_cat/fielddata?v&fields=log*
# 路径参数
GET /_cat/fielddata/log*?v
```

## [cat health](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-health.html)

```shell
# 集群健康查询
GET /_cat/health?v
# 不显示时间戳字段
GET /_cat/health?v&ts=false

```

## [cat indices](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-indices.html)

```shell
# 索引基本信息(文档数,大小,分片数)查看
GET /_cat/indices/log*?v&s=index
# 按文档数逆序排序
GET /_cat/indices/log*?v&s=docs.count:desc
# 索引进行了多少次段合并
GET /_cat/indices/log*?pri&v&h=health,index,pri,rep,docs.count,mt
# 索引占用内存信息查看
GET /_cat/indices?v&h=i,tm&s=tm:desc
# 查看索引全部相信信息
GET /_cat/indices/logstash-spring-es-dev-2019.04.08?h=*&format=yaml
```

## [cat master](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-master.html)

```shell
# 查看集群master是谁
GET /_cat/master?v
```

## [cat nodeattrs](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-nodeattrs.html)

```shell
# 查看节点属性信息
GET /_cat/nodeattrs?v
```

## [cat nodes](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-nodes.html)

```shell
# 查看节点基本信息
GET /_cat/nodes
# 全部信息展示
GET /_cat/nodes?h=*&format=yaml
```

## [cat pending tasks](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-pending-tasks.html)

```shell
# 查看执行中的任务
GET /_cat/pending_tasks?v
```

## [cat plugins](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-plugins.html)

```shell
# 插件信息查看
GET /_cat/plugins
# 指定字段
GET /_cat/plugins?v&s=component&h=name,component,version,description
```

## [cat recovery](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-recovery.html)

> A recovery event occurs anytime an index shard moves to a different node in the cluster. This can happen during a snapshot recovery, a change in replication level, node failure, or on node startup. 

```shell
# 查看恢复信息
GET _cat/recovery?v
# 增加节点触发的恢复,type=peer
GET _cat/recovery?v&h=i,s,t,ty,st,shost,thost,f,fp,b,bp
# 快照恢复
GET _cat/recovery?v&h=i,s,t,ty,st,rep,snap,f,fp,b,bp
```

## [cat repositories](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-repositories.html)

```shell
# 查看集群中有哪些快照仓库
GET /_cat/repositories?v
```

## [cat thread pool](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-thread-pool.html)

```shell
# 线程池信息
GET /_cat/thread_pool
# 查看某个池的信息(支持*和,)
GET /_cat/thread_pool/search?v&h=node_name,name,size,active,queue,largest,rejected,completed
```

## [cat shards](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-shards.html)

```shell
# 分片信息查看
GET _cat/shards?v
# 模糊匹配(支持*和,)
GET _cat/shards/log*?v
# 分片无法分配原因查看
GET _cat/shards?v&h=index,shard,prirep,state,unassigned.reason
```

* These are the possible reasons for a shard to be in a unassigned state:

| 错误码                    | 解释                                                         |
| ------------------------- | ------------------------------------------------------------ |
| `INDEX_CREATED`           | Unassigned as a result of an API creation of an index.       |
| `CLUSTER_RECOVERED`       | Unassigned as a result of a full cluster recovery.           |
| `INDEX_REOPENED`          | Unassigned as a result of opening a closed index.            |
| `DANGLING_INDEX_IMPORTED` | Unassigned as a result of importing a dangling index.        |
| `NEW_INDEX_RESTORED`      | Unassigned as a result of restoring into a new index.        |
| `EXISTING_INDEX_RESTORED` | Unassigned as a result of restoring into a closed index.     |
| `REPLICA_ADDED`           | Unassigned as a result of explicit addition of a replica.    |
| `ALLOCATION_FAILED`       | Unassigned as a result of a failed allocation of the shard.  |
| `NODE_LEFT`               | Unassigned as a result of the node hosting it leaving the cluster. |
| `REROUTE_CANCELLED`       | Unassigned as a result of explicit cancel reroute command.   |
| `REINITIALIZED`           | When a shard moves from started back to initializing, for example, with shadow replicas. |
| `REALLOCATED_REPLICA`     | A better replica location is identified and causes the existing replica allocation to be cancelled. |

## [cat segments](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-segments.html)

```shell
# 段信息查看
GET /_cat/segments?v
# 查看索引段信息(支持*和,)
GET /_cat/segments/pdm_doc?v

```

## [cat snapshots](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-snapshots.html)

```shell
# repo查询
GET _cat/repositories
# 快照查询
GET /_cat/snapshots/hdfs_repository_test?v&s=id

```

## [cat templates](https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-templates.html)

```shell
# 模板查询
GET /_cat/templates?v&s=name
# 模糊搜索(支持*和,)
GET /_cat/templates/log*?v&s=name
```

