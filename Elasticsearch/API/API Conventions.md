## [通用选项](https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html)
### Pretty Result
* 格式化json输出
```
?pretty=true
```
* 输出改为yaml格式
```  
?format=yaml
```
### Human readable output
* 默认值为false
```markdown
?human
```
### Date Math

> 范围查询的gt,lt和聚合中datarange的from,to都能够识别date  math

表达式以一个 `anchor date`开始, `anchor date `可以是`now`或者一个以 `||`结尾的日期字符串. `anchor date`可以被以下表达式操作:

- `+1h`: 加一小时
- `-1d`: 减一天
- `/d`: 向下取整到天,(截断)

支持的单位:

| 单位 | 解释    |
| ---- | ------- |
| `y`  | Years   |
| `M`  | Months  |
| `w`  | Weeks   |
| `d`  | Days    |
| `h`  | Hours   |
| `H`  | Hours   |
| `m`  | Minutes |
| `s`  | Seconds |

Assuming `now` is `2001-01-01 12:00:00`, some examples are:

| `now+1h`              | `now` in milliseconds plus one hour. Resolves to: `2001-01-01 13:00:00` |
| --------------------- | ------------------------------------------------------------ |
| `now-1h`              | `now` in milliseconds minus one hour. Resolves to: `2001-01-01 11:00:00` |
| `now-1h/d`            | `now` in milliseconds minus one hour, rounded down to UTC 00:00. Resolves to: `2001-01-01 00:00:00` |
| `2001.02.01\|\|+1M/d` | `2001-02-01` in milliseconds plus one month. Resolves to: `2001-03-01 00:00:00` |

### Response Filtering

* 只显示took,hits_id,hits_score字段   
```
GET /_search?q=elasticsearch&filter_path=took,hits.hits._id,hits.hits._score
```
* 使用通配符 
```markdown
GET /_cluster/state?filter_path=metadata.indices.*.stat*
```
* 使用 ** 通配符,忽略上级是什么  
```
GET /_cluster/stats?filter_path=**.version&human
```
* 使用 - 号排除字段  
```
GET /_count?filter_path=-_shards
```


