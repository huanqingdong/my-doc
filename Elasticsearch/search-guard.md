* [search gurad 插件](https://docs.search-guard.com/latest/search-guard-installation)
```
# 启用 enable-shard-allocation
./sgadmin.sh --enable-shard-allocation -cacert /home/elk/module/tlstool/out/root-ca.pem -cert /home/elk/module/tlstool/out/faith.pem -key /home/elk/module/tlstool/out/faith.key -keypass 1rnfiuIMtcGu -h 10.0.11.213 -cn test-application 
# 初始化
./sgadmin.sh -cd ../sgconfig/ -icl -nhnv -cacert /home/elk/module/tlstool/out/root-ca.pem -cert /home/elk/module/tlstool/out/faith.pem -key /home/elk/module/tlstool/out/faith.key -keypass 1rnfiuIMtcGu -h 10.0.11.213 
# 刷新配置
./sgadmin.sh -rl -icl -cacert /home/elk/module/tlstool/out/root-ca.pem -cert /home/elk/module/tlstool/out/faith.pem -key /home/elk/module/tlstool/out/faith.key -keypass 1rnfiuIMtcGu -h 10.0.11.213 

```
* [TLS Tool](https://docs.search-guard.com/latest/offline-tls-tool#node-and-client-certificates)
```markdown
# 生成ca + crt
/tools/sgtlstool.sh -c ../config/tlsconfig.yml -ca -crt
# 只生成 ca
/tools/sgtlstool.sh -c ../config/tlsconfig.yml -ca
# 只生成 crt, 添加证书时使用
/tools/sgtlstool.sh -c ../config/tlsconfig.yml -crt

```
* [Search Guard Kibana plugin](https://docs.search-guard.com/latest/kibana-plugin-installation)
```markdown
# 插件安装
Execute:  NODE_OPTIONS="--max-old-space-size=8192" bin/kibana-plugin install file:///home/elk/software/search-guard-kibana-plugin-6.5.3-17.zip
```
```mermaid
graph TD
   A --> B
```
