### [How to resolve unassigned shards in Elasticsearch](https://www.datadoghq.com/blog/elasticsearch-unassigned-shards/#reason-1-shard-allocation-is-purposefully-delayed)

* Shard allocation is purposefully delayed

  节点离开集群后,在一钟以内,不会重分shard,以保证节点能够再次连接

  ```shell
  # 可通过以下参数修改重分时间
  curl -XPUT 'localhost:9200/<INDEX_NAME>/_settings' -d 
  '{
  	"settings": {
  	   "index.unassigned.node_left.delayed_timeout": "30s"
  	}
  }'
  # <INDEX_NAME>替换为_all将对集群中所有索引生效
  ```

  

* Too many shards, not enough nodes

  有太多分片,没有足够的节点,分片数大于节点数

* You need to re-enable shard allocation

* Shard data no longer exists in the cluster

* Low disk watermark

  默认情况下,磁盘剩余少于85%,则将停止分配shard
  ```shell
  # 检查各节点磁盘剩余
  GET _cat/allocation?v
  # 提升百分比到90%
  PUT _cluster/settings
  {
    "transient": {	
  	      "cluster.routing.allocation.disk.watermark.low": "90%"
  	}
  }
  ```

* Multiple Elasticsearch versions

