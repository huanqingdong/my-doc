* 参考官方文档

  https://www.elastic.co/guide/en/elasticsearch/reference/current/sql-client-apps-dbeaver.html

* maven仓库指定
  https://artifacts.elastic.co/maven

* SSL连接需加配置项

  ```conf
  # 编辑驱动设置->连接属性
  # 1. 开启ssl,或者使用https
  # jdbc:es://[[http|https]://]*[host[:port]]*/[prefix]*[?[option=value]&]*
  ssl=true
  # 指定key文件
  ssl.keystore.location=D:\home\dft\module\cert\test.jks
  # 指定key密码
  ssl.keystore.pass=123456a?
  # 指定根证书位置,用于验证服务器证书是否可信,
  ssl.truststore.location=D:\home\dft\module\cert\truststore.jks
  ```

  

