### 1. 安装环境

操作系统:  CentOS Linux release 7.2.1511 (Core) 

elasticsearch: 6.6.2

jdk: jdk-8u201

###  2.  jdk安装

  * tar.gz

  ```shell
# 下载
wget http://10.0.11.22/elk/jdk-8u201-linux-x64.tar.gz
# 解压
sudo tar -xzvf jdk-8u201-linux-x64.tar.gz -C /opt/
# 配置java_home 
sudo vim /etc/profile
export JAVA_HOME=/opt/jdk1.8.0_201
export PATH=$JAVA_HOME/bin:$PATH
 
  ```

### 3. elasticsearch安装
  * 软件下载

  ```shell
  # 官网下载
  wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.2.rpm
  wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.2.rpm.sha512
  # 自建http服务器
  wget http://10.0.11.22/elk/elasticsearch-6.6.2.rpm
  wget http://10.0.11.22/elk/elasticsearch-6.6.2.rpm.sha512
  # 安装sha工具,并验证文件sha值, elasticsearch-6.6.2.rpm: OK表示文件无误
  yum install -y perl-Digest-SHA
  shasum -a 512 elasticsearch-6.6.2.rpm -c elasticsearch-6.6.2.rpm.sha512 
  ```

* 安装

  ```shell
  rpm -ivh elasticsearch-6.6.2.rpm
  
  ```

* 配置

  * tar.gz方式安装的jdk需要配置JAVA_HOME

    ```shell
    vim /etc/sysconfig/elasticsearch
    
    JAVA_HOME=/opt/jdk1.8.0_201
    ```

  * 数据和日志目录创建

    ```shell
    mkdir -vp /home/elasticsearch/{data,logs}
    chown elasticsearch:elasticsearch /home/elasticsearch/ -R
    ```

    

  * vim /etc/elasticsearch/elasticsearch.yml

    ```properties
    # ip设置
    network.host: 10.0.11.22
    ```

  * 协调节点配置

    ```shell
    vim /etc/elasticsearch/elasticsearch.yml
    
    node.name: Coordinating-210
    node.master: false
    node.data: false
    node.ingest: false
    ```

  * JVM配置

    ```shell
    # jvm添加如下配置
    vim /etc/elasticsearch/jvm.options
    -Xms30g
    -Xmx30g
    -XX:+UnlockDiagnosticVMOptions
    -XX:+PrintCompressedOopsMode
    # 直接执行shell,log4j文件日志不会记录此信息
    /usr/share/elasticsearch/bin/elasticsearch
    # 找到最大的 zero based Compressed Oops,一般为26G~30G之间
    heap address: 0x000000011be00000, size: 27648 MB, zero based Compressed Oops
    ```

    

* 启动

  ```shell
  # 设置开机启动
  systemctl daemon-reload
  systemctl enable elasticsearch.service
  # 启动或停止
  systemctl start elasticsearch.service
  systemctl stop elasticsearch.service
  # 查看启动状态
  systemctl status elasticsearch.service
  ```

  

* Directory layout of RPM

  The RPM places config files, logs, and the data directory in the appropriate locations for an RPM-based system:

  | Type        | Description                                                  | Default Location                   | Setting        |
  | ----------- | ------------------------------------------------------------ | ---------------------------------- | -------------- |
  | **home**    | Elasticsearch home directory or `$ES_HOME`                   | `/usr/share/elasticsearch`         |                |
  | **bin**     | Binary scripts including `elasticsearch` to start a node and `elasticsearch-plugin` to install plugins | `/usr/share/elasticsearch/bin`     |                |
  | **conf**    | Configuration files including `elasticsearch.yml`            | `/etc/elasticsearch`               | `ES_PATH_CONF` |
  | **conf**    | Environment variables including heap size, file descriptors. | `/etc/sysconfig/elasticsearch`     |                |
  | **data**    | The location of the data files of each index / shard allocated on the node. Can hold multiple locations. | `/var/lib/elasticsearch`           | `path.data`    |
  | **logs**    | Log files location.                                          | `/var/log/elasticsearch`           | `path.logs`    |
  | **plugins** | Plugin files location. Each plugin will be contained in a subdirectory. | `/usr/share/elasticsearch/plugins` | ``             |
  | **repo**    | Shared file system repository locations. Can hold multiple locations. A file system repository can be placed in to any subdirectory of any directory specified here. |                                    |                |

### 4. search-guard安装

#### 4.1 安装前准备

* 使用官方[TLS Tool](https://docs.search-guard.com/latest/offline-tls-tool)生成所需的证书

  ```shell
  # 下载tlstool
  # 官方下载
  wget https://search.maven.org/remotecontent?filepath=com/floragunn/search-guard-tlstool/1.6/search-guard-tlstool-1.6.tar.gz
  # 自建http服务下载
  wget http://10.0.11.22/elk/search-guard-tlstool-1.6.tar.gz
  # 解压
  mkdir /opt/tlstool & tar -xzvf search-guard-tlstool-1.6.tar.gz -C /opt/tlstool
  # 复制示例文件到tlsconfig.yml
  cp /opt/tlstool/config/example.yml /opt/tlstool/config/tlsconfig.yml
  ```

* 修改配置文件 tlsconfig.yml

  ```properties
  ###
  ### Self-generated certificate authority
  ### 
  # 
  # If you want to create a new certificate authority, you must specify its parameters here. 
  # You can skip this section if you only want to create CSRs
  #
  ca:
     root:
        # The distinguished name of this CA. You must specify a distinguished name.   
        dn: CN=root.ca.weichai.com,OU=CA,O=weichai Com\, Inc.,DC=weichai,DC=com
  
        # The size of the generated key in bits
        keysize: 2048
  
        # The validity of the generated certificate in days from now
        validityDays: 3650
        
        # Password for private key
        #   Possible values: 
        #   - auto: automatically generated password, returned in config output; 
        #   - none: unencrypted private key; 
        #   - other values: other values are used directly as password   
        pkPassword: pass_rootca 
        
        # The name of the generated files can be changed here
        file: root-ca.pem
        
  ### 
  ### Default values and global settings
  ###
  defaults:
  
        # The validity of the generated certificate in days from now
        validityDays: 3650 
        
        # Password for private key
        #   Possible values: 
        #   - auto: automatically generated password, returned in config output; 
        #   - none: unencrypted private key; 
        #   - other values: other values are used directly as password   
        pkPassword: pass_dev      
        
        # Specifies to recognize legitimate nodes by the distinguished names
        # of the certificates. This can be a list of DNs, which can contain wildcards.
        # Furthermore, it is possible to specify regular expressions by
        # enclosing the DN in //. 
        # Specification of this is optional. The tool will always include
        # the DNs of the nodes specified in the nodes section.            
        #nodesDn:
        #- "CN=*.example.com,OU=Ops,O=Example Com\\, Inc.,DC=example,DC=com"
        # - 'CN=node.other.com,OU=SSL,O=Test,L=Test,C=DE'
        # - 'CN=*.example.com,OU=SSL,O=Test,L=Test,C=DE'
        # - 'CN=elk-devcluster*'
        # - '/CN=.*regex/' 
  
        # If you want to use OIDs to mark legitimate node certificates, 
        # the OID can be included in the certificates by specifying the following
        # attribute
        
        # nodeOid: "1.2.3.4.5.5"
  
        # The length of auto generated passwords            
        generatedPasswordLength: 12
        
        # Set this to true in order to generate config and certificates for 
        # the HTTP interface of nodes
        httpsEnabled: true
        
        # Set this to true in order to re-use the node transport certificates
        # for the HTTP interfaces. Only recognized if httpsEnabled is true
        
        # reuseTransportCertificatesForHttp: false
        
        # Set this to true to enable hostname verification
        #verifyHostnames: false
        
        # Set this to true to resolve hostnames
        #resolveHostnames: false
        
        
  ###
  ### Nodes
  ###
  #
  # Specify the nodes of your ES cluster here
  #      
  nodes:
    - name: node-22
      dn: CN=node-22.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      dns: cdhdev01.weichai.com
      ip: 10.0.11.22
    - name: node-23
      dn: CN=node-23.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      dns: cdhdev02.weichai.com
      ip: 10.0.11.23
    - name: node-24
      dn: CN=node-24.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      dns: cdhdev0.weichai.com
      ip: 10.0.11.24
    - name: node-25
      dn: CN=node-25.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      dns: cdhdev04.weichai.com
      ip: 10.0.11.25
  
  ###
  ### Clients
  ###
  #
  # Specify the clients that shall access your ES cluster with certificate authentication here
  #
  # At least one client must be an admin user (i.e., a super-user). Admin users can
  # be specified with the attribute admin: true    
  #        
  clients:
    # 管理用户
    - name: adminDev
      dn: CN=adminDev.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      admin: true
      pkPassword: 123456a?
    # 程序连接用
    - name: dev
      dn: CN=dev.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      pkPassword: 123456a?
  
  ```

* TLS Tool命令

  ```shell
  ## 在tlstool目录下执行,重复执行不会覆盖
  # 只生成 ca
  ./tools/sgtlstool.sh -c ./config/tlsconfig.yml -ca
  # 只生成 crt, 添加证书时使用
  ./tools/sgtlstool.sh -c ./config/tlsconfig.yml -crt
  # 生成ca + crt
  ./tools/sgtlstool.sh -c ./config/tlsconfig.yml -ca -crt
  ```

#### 4.2 安装

```shell
# 1. 停止分片分配
PUT _cluster/settings
{
  "persistent": {
    "cluster.routing.allocation.enable": "none"
  }
}
# 2. 下载对应版本的插件
https://docs.search-guard.com/latest/search-guard-versions
# 3. 安装
/usr/share/elasticsearch/bin/elasticsearch-plugin install http://10.0.11.22/elk/search-guard-6-6.6.2-24.2.zip
# 4. 添加tls配置
  # 将各机器对应的证书和root-ca 复制到个机器的/etc/elasticsearch 下
  scp -r node-22* dev01:/etc/elasticsearch/
  scp root-ca.pem dev01:/etc/elasticsearch/
  # 将adminDev证书复制到/etc/elasticsearch/
  cp /opt/tlstool/out/adminDev* /usr/share/elasticsearch/plugins/search-guard-6/tools/
  cp /opt/tlstool/out/root-ca.pem /usr/share/elasticsearch/plugins/search-guard-6/tools/
  # 除了tlstool生成的配置,还需添加一下配置
  # 禁用xpack安全
xpack.security.enabled: false
# 允许具有snapshotrestore权限的账号进行快照恢复
searchguard.enable_snapshot_restore_privilege: true


# 5. 开启分片分配,
  # 为search-guard脚本添加执行权限
      cd /usr/share/elasticsearch/plugins/search-guard-6/tools
      chmod +x *.sh
  # 在elasticsearch/plugins/search-guard-6/tools目录执行以下脚本,启用 enable-shard-allocation
  ./sgadmin.sh --enable-shard-allocation -cacert root-ca.pem -cert adminDev.pem -key adminDev.key -keypass 123456a? -h 10.0.11.24 -cn es-dev

# 6. 初始化
./sgadmin.sh -cd ../sgconfig/ -icl -nhnv -cacert root-ca.pem -cert adminDev.pem -key adminDev.key -keypass 123456a? -h 10.0.11.24
# 7. 检查状态
curl -k https://10.0.11.24:9200/_searchguard/health??pretty
以下返回表示成功
{
  "message" : null,
  "mode" : "strict",
  "status" : "UP"
}

# 密码修改处理
kibanaserver_pspx
# 获取密码hash
.hash.sh -p 123456a?
# 1.解除只读限制
 PUT /searchguard/_settings
 {
   "index.blocks.read_only_allow_delete": null
 }
# 2. 初始化
./sgadmin.sh -cd ../sgconfig/ -icl -nhnv -cacert root-ca.pem -cert adminDev.pem -key adminDev.key -keypass 123456a? -h 10.0.11.24
# 3. 刷新配置
./sgadmin.sh -rl -icl -cacert root-ca.pem -cert adminDev.pem -key adminDev.key -keypass 123456a? -h 10.0.11.24
# 刷新异常 [FORBIDDEN/12/index read-only / allow delete (api)];] 解决
PUT searchguard/_settings
{
  "index" : {
    "blocks" : {
      "read_only_allow_delete" : null
    }
  }
}



```

#### 4.3 配置程序访问用户

* 在tlsconfig.yml中添加配置

  ```properties
  # 用户名为dev,密码为123456a?
  clients:
    # 程序连接用
    - name: dev
      dn: CN=dev.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com
      pkPassword: 123456a?
  ```

* 生成所需的key和pem文件

  ```shell
  ./tools/sgtlstool.sh -c ./config/tlsconfig.yml -crt
  ```

* 配置用户权限,配置文件在插件目录

  ```properties
  # 在文件sg_internal_users.yml 添加用户,hash值使用 hash.sh -p 123456a?生成
  # 自定义用户
  CN=dev.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com:
    hash: $2y$12$FVG0ojaqLdLJt9ltKvo8huATZyTVZroN6eMAkeW52yfDu9oVwl.pS
  # 在文件sg_roles_mapping.yml 中配置用户权限,如下为加到sg_readall角色
  sg_readall:
    readonly: true
    backendroles:
      - readall
    users:
      - 'CN=dev.weichai.com,OU=Ops,O=weichai Com\, Inc.,DC=weichai,DC=com'
  ```

* 刷新配置

  执行postAndRefresh.sh脚本,内容如下:

  ```shell
  ./sgadmin.sh -cd ../sgconfig/ -icl -nhnv -cacert root-ca.pem -cert adminDev.pem -key adminDev.key -keypass 123456a? -h 10.0.11.24
  ./sgadmin.sh -rl -icl -cacert root-ca.pem -cert adminDev.pem -key adminDev.key -keypass 123456a? -h 10.0.11.24
  ```

#### 4.4 kibana 插件安装

```shell
# 1. 下载对应版本的插件
https://docs.search-guard.com/latest/search-guard-versions
# 2. 停止kibana服务
systemctl stop kibana
# 3. 安装插件
NODE_OPTIONS="--max-old-space-size=8192"
/usr/share/kibana/bin/kibana-plugin  install http://10.0.11.22/elk/search-guard-kibana-plugin-6.6.2-18.1.zip
# 以下目录需要手动改为kibana用户,真是扯淡
/usr/share/kibana/optimize/bundles/src
chown -R kibana:kibana /usr/share/kibana/optimize/bundles/src

# 4. 配置
vim /etc/kibana/kibana.yml
xpack.security.enabled: false
# 账号密码
elasticsearch.username: "kibanaserver"
elasticsearch.password: "kibanaserver"
# 使用https
elasticsearch.url: "https://10.0.11.22:9200"
# root-ca 
elasticsearch.ssl.certificateAuthorities:  [ "/etc/elasticsearch/root-ca.pem" ]

# 5. 配置https连接(rpm安装,有些日志无法打印,直接启动bin下的kibana查看日志)
server.ssl.enabled: true
server.ssl.certificate: /etc/kibana/weichai.pem
server.ssl.key: /etc/kibana/weichai.key
server.ssl.keyPassphrase: 123456a?

```

### 5. Ik安装
```shell
# 下载 
https://github.com/medcl/elasticsearch-analysis-ik/releases
# 安装
/usr/share/elasticsearch/bin/elasticsearch-plugin install  http://10.0.11.22/elk/elasticsearch-analysis-ik-6.6.2.zip
# 配置
vim /etc/elasticsearch/analysis-ik/IKAnalyzer.cfg.xml
	<entry key="remote_ext_dict">http://10.0.8.121/ik/mydict.txt</entry>
    <entry key="remote_ext_stopwords">http://10.0.8.121/ik/stopword.txt</entry>
    
# 发送远程节点
scp IKAnalyzer.cfg.xml dev01://etc/elasticsearch/analysis-ik/
```

### 6. Reopsitory-hdfs 安装

```shell
# 下载
https://www.elastic.co/guide/en/elasticsearch/plugins/current/repository-hdfs.html
# 安装
/usr/share/elasticsearch/bin/elasticsearch-plugin install  http://10.0.11.22/elk/repository-hdfs-6.6.2.zip

# 执行恢复时无权限,需为admin账号添加snapshotrestore
修改 /usr/share/elasticsearch/plugins/search-guard-6/sgconfig/sg_internal_users.yml
执行 /usr/share/elasticsearch/plugins/search-guard-6/tools/postAndRefresh.sh
```



