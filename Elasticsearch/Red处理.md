```shell
GET _cluster/health

GET _cat/health
GET _cat/indices?v&health=red

GET _cat/shards?v
GET _cat/shards?v&index=.monitoring-kibana-6-2019.03.28

DELETE logstash-es-test-spring-es-dev-2019.03.27

GET _cluster/settings

PUT _cluster/settings
{
  "persistent": {
    "cluster.routing.allocation.enable": null
  }
}

GET logstash-es-test-spring-es-dev-2019.03.27/_settings

POST _cluster/reroute
{
"commands" : [
      
      {
        "allocate_empty_primary" : {
              "index" : "logstash-es-test-spring-es-test-2019.03.27", "shard" : 4,
              "node" : "node-213",
              "accept_data_loss":true
        }
      }
    ]
}

```

