## 下载

* 去阿里云下载dockertoolbox

  http://mirrors.aliyun.com/docker-toolbox/windows/docker-toolbox/

* 去github下载boot2docker.iso

  https://github.com/boot2docker/boot2docker/releases

## 安装

* 安装dockertoolbox

  安装完成后

  1. 将boot2docker.iso放到C:\Users\Lenovo\.docker\machine\cache\boot2docker.iso

  2. 点击Docker Quickstart Terminal

     如果报找不到VBoxManage.exe

     编辑 C:\Program Files\Docker Toolbox\start.sh

     设置 VBOXMANAGE="C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"

