## spring-es

```shell
version: "2.2"
services:
  spring-es:
    image: 10.0.8.121:1123/spring-es-test
    container_name: spring-es-test01
    hostname: spring-es-test01
    privileged: true
    environment:
      PARAMS: --spring.profiles.active=dev
      #JAVA_OPTS: -Xms3G -Xmx3G
    #    volumes:
    #      - ./krb5.conf:/opt/module/kafka-manager/conf/krb5.conf
    #      - ./zhihuiyun.keytab:/opt/module/kafka-manager/conf/zhihuiyun.keytab
    ports:
      - 9988:9988
    extra_hosts:
      - "cdhuat01.weichai.com:10.0.11.91"
      - "cdhuat02.weichai.com:10.0.11.92"
      - "cdhuat03.weichai.com:10.0.11.93"
      - "cdhuat04.weichai.com:10.0.11.94"
      - "cdhdev01.weichai.com:10.0.11.22"
      - "cdhdev02.weichai.com:10.0.11.23"
      - "cdhdev03.weichai.com:10.0.11.24"
      - "cdhdev04.weichai.com:10.0.11.25"

# 需自建网络
# docker network create --subnet=12.12.1.0/16 --gateway 12.12.1.1 net-12
networks:
  default:
    external:
      name: net-12

# 定义网段
networks:      
  net-zabbix:
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      config:
      - subnet: 16.16.16.0/24
      

```

