```shell
# 下载centos镜像
docker pull centos
# 运行镜像
docker run -it -v /root/soft/:/root/soft/ centos /bin/bash

# Dockerfile 编写
#插入以下内容
#使用的基础镜像
FROM centos:7.6.1810
#作者信息
MAINTAINER huanqingdong "huanqingdong@weichai.com"

RUN yum -y install iproute

#创建目录
RUN mkdir -p /opt/module

# jdk
ADD jdk1.8.0_201 /opt/module/jdk1.8.0_201
# zookeeper
ADD zookeeper-3.4.9 /opt/module/zookeeper-3.4.9
#把当前目录下的jdk文件夹添加到镜像
ADD kafka-manager /opt/module/kafka-manager

#添加环境变量
ENV JAVA_HOME /opt/module/jdk1.8.0_201
ENV PATH $PATH:$JAVA_HOME/bin

#暴露8080端口
EXPOSE 9000
EXPOSE 2181

#启动时运行tomcat
CMD ["/opt/module/zookeeper-3.4.9/start.sh","run"]
CMD ["/opt/module/kafka-manager/rm.sh","run"]

```

```shell
## 纯净jdk1.8.0_201
rm -rf ./jdk1.8.0_201/*src.zip \
    ./jdk1.8.0_201/lib/missioncontrol \
    ./jdk1.8.0_201/lib/visualvm \
    ./jdk1.8.0_201/lib/*javafx* \
    ./jdk1.8.0_201/jre/lib/plugin.jar \
    ./jdk1.8.0_201/jre/lib/ext/jfxrt.jar \
    ./jdk1.8.0_201/jre/bin/javaws \
    ./jdk1.8.0_201/jre/lib/javaws.jar \
    ./jdk1.8.0_201/jre/lib/desktop \
    ./jdk1.8.0_201/jre/plugin \
    ./jdk1.8.0_201/jre/lib/deploy* \
    ./jdk1.8.0_201/jre/lib/*javafx* \
    ./jdk1.8.0_201/jre/lib/*jfx* \
    ./jdk1.8.0_201/jre/lib/amd64/libdecora_sse.so \
    ./jdk1.8.0_201/jre/lib/amd64/libprism_*.so \
    ./jdk1.8.0_201/jre/lib/amd64/libfxplugins.so \
    ./jdk1.8.0_201/jre/lib/amd64/libglass.so \
    ./jdk1.8.0_201/jre/lib/amd64/libgstreamer-lite.so \
    ./jdk1.8.0_201/jre/lib/amd64/libjavafx*.so \
    ./jdk1.8.0_201/jre/lib/amd64/libjfx*.so


```

```yaml
# docker-compose.yml
version: "2.2"
services:
  kafka-manager:
    image: huanqingdong/kafka-manager:3.21
    container_name: kafka-manager
    privileged: true
    volumes:
      - ./krb5.conf:/opt/module/kafka-manager/conf/krb5.conf
      - ./zhihuiyun.keytab:/opt/module/kafka-manager/conf/zhihuiyun.keytab
    ports:
      - 9000:9000
    extra_hosts:
      - "cdhuat01.weichai.com:10.0.11.91"
      - "cdhuat02.weichai.com:10.0.11.92"
      - "cdhuat03.weichai.com:10.0.11.93"
      - "cdhuat04.weichai.com:10.0.11.94"
```

## 镜像构建
基于 CentOS Linux release 7.6.1810 镜像构建
JDK版本:1.8.0_201
zookeeper:版本3.4.9
kafka-manager版本:1.3.3.21
* Dockerfile
```conf
#插入以下内容
#使用的基础镜像
FROM centos
#作者信息
MAINTAINER huanqingdong "huanqingdong@weichai.com"
RUN yum -y install iproute
#创建目录
RUN mkdir -p /opt/module
# jdk
ADD jdk1.8.0_201 /opt/module/jdk1.8.0_201
# zookeeper
ADD zookeeper-3.4.9 /opt/module/zookeeper-3.4.9
#把当前目录下的jdk文件夹添加到镜像
ADD kafka-manager /opt/module/kafka-manager
#添加环境变量
ENV JAVA_HOME /opt/module/jdk1.8.0_201
ENV PATH $PATH:$JAVA_HOME/bin
#暴露8080端口
EXPOSE 9000
EXPOSE 2181
#启动时运行tomcat
#CMD ["nohup /opt/module/zookeeper-3.4.9/start.sh >zk.log 2>&1 &","run"]
CMD ["/opt/module/kafka-manager/rm.sh","run"]
```


## 快速启动
```shell
docker run -d -p 9000:9000 --name kafka-manager huanqingdong/kafka-manager:3.21
```
### kerberos环境下kafka连接
* 配置文件准备
```shell
# 复制krb5和keytab文件到kafka-manager/conf下
docker cp /etc/krb5.conf kafka-manager:/opt/module/kafka-manager/conf/
docker cp /root/zhihuiyun.keytab kafka-manager:/opt/module/kafka-manager/conf/
```

* 开启kerberos的kafka添加
```shell
# Security Protocol 
SASL_PLAINTEXT
# SASL Mechanism
GSSAPI
# SASL JAAS Config
com.sun.security.auth.module.Krb5LoginModule required useKeyTab=true keyTab="/opt/module/kafka-manager/conf/zhihuiyun.keytab" principal="zhihuiyun" serviceName="kafka";
```

## docker-compose
### 连接开启kerberos的kafka
* docker-compose.yml
```java
version: "2.2"
services: 
    kafka-manager:
      image: huanqingdong/kafka-manager:3.21
      container_name: kafka-manager
      privileged: true
      volumes:
        - ./krb5.conf:/opt/module/kafka-manager/conf/krb5.conf
        - ./zhihuiyun.keytab:/opt/module/kafka-manager/conf/zhihuiyun.keytab
      ports:
        - 9000:9000
      extra_hosts:
        - "cdhuat01.weichai.com:10.0.11.91"
        - "cdhuat02.weichai.com:10.0.11.92"
        - "cdhuat03.weichai.com:10.0.11.93"
        - "cdhuat04.weichai.com:10.0.11.94"
        - "cdhdev01.weichai.com:10.0.11.22"
        - "cdhdev02.weichai.com:10.0.11.23"
        - "cdhdev03.weichai.com:10.0.11.24"
        - "cdhdev04.weichai.com:10.0.11.25"

```
* 添加配置项
```shell
# Security Protocol 
SASL_PLAINTEXT
# SASL Mechanism
GSSAPI
# SASL JAAS Config
com.sun.security.auth.module.Krb5LoginModule required useKeyTab=true keyTab="/opt/module/kafka-manager/conf/zhihuiyun.keytab" principal="zhihuiyun" serviceName="kafka";
```