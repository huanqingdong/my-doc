## NEXUS

### [nexus安装](<https://hub.docker.com/r/sonatype/nexus3>)

```shell
# 创建存放数据的volume
docker volume create --name nexus-data
# 查看nexus-data的本地磁盘映射情况
docker volume inspect nexus-data
# 通过查看,nexus-data映射到本地磁盘/home/docker/volumes/nexus-data/_data
# 数据迁移
cp -R ./sonatype/nexus3/* /home/docker/volumes/nexus-data/_data
chown -R 200:200 /home/docker/volumes/nexus-data/_data

# 启动docker容器 (8082,8083用于docker镜像端口)
docker run -d -p 1122:8081 -p 1123:8082 -p 1124:8083 --restart always --name nexus -v nexus-data:/nexus-data -e NEXUS_CONTEXT=nexus sonatype/nexus3
# --restart always 总是随docker启动而启动容器
# --name 容器名称
# -v 使用创建的nexus-data volume映射到容器中的/nexus-data路径
# NEXUS_CONTEXT=nexus 指定web的context,默认为/
# 最后面为镜像名称

```

### [nexus docker 代理 ](https://www.jianshu.com/p/cf6b38084304)

```shell
# 代理使用
{   
	"registry-mirrors": ["http://10.0.8.121:1124"],
	"insecure-registries":["10.0.8.121:1123"]
}

# 镜像推送
# 1. 登入nexus
docker login 10.0.8.121:1123
# 2. tag镜像
docker tag mysql 10.0.8.121:1123/mysql
# 3. push镜像
docker push 10.0.8.121:1123/mysql

```

### ldap配置

```shell
# LDAP server address
ldap://10.0.6.101:389
# Search base:
DC=weichai,DC=com
# Authentlcation method
Simple Authentlcation
# username or Dn
CN=s-plan,OU=服务帐户,OU=服务帐户,DC=weichai,DC=com
# Password
123456a?

# Click Next
# Configuration template
Active Directory
# Base DN
OU=潍柴动力股份有限公司,OU=潍柴用户
# User subtree
勾选 Are users loacted in structures below the user base DN?
# Click Create
```

## ZABBIX

```shell
version: "2.2"
services:
  zabbix-mysql:
    image: mysql:5.7
    container_name: zabbix-mysql
    privileged: true
    environment:
      MYSQL_DATABASE: zabbix
      MYSQL_USER: zabbix
      MYSQL_PASSWORD: zabbix
      MYSQL_ROOT_PASSWORD: root
    volumes:
      - mysql:/var/lib/mysql
    ports:
      - 13306:3306
  zabbix-server:
    image: zabbix/zabbix-server-mysql:centos-4.0-latest
    container_name: zabbix-server
    privileged: true
    links: 
      - zabbix-mysql
    environment:
      DB_SERVER_HOST: zabbix-mysql
      MYSQL_DATABASE: zabbix
      MYSQL_USER: zabbix
      MYSQL_PASSWORD: zabbix
      MYSQL_ROOT_PASSWORD: root
    volumes:
      - zabbix:/var/lib/zabbix
    ports:
      - 10051:10051
  zabbix-web:
    image: zabbix/zabbix-web-nginx-mysql:centos-4.0-latest
    container_name: zabbix-web
    privileged: true
    links: 
      - zabbix-mysql
      - zabbix-server
    environment:
      ZBX_SERVER_HOST: zabbix-server
      DB_SERVER_HOST: zabbix-mysql
      MYSQL_DATABASE: zabbix
      MYSQL_USER: zabbix
      MYSQL_PASSWORD: zabbix
      PHP_TZ: Asia/Shanghai
    ports:
      - 1080:80
  zabbix-agent:
    image: zabbix/zabbix-agent:centos-4.0-latest
    container_name: zabbix-agent
    environment:
      ZBX_HOSTNAME: localhost
      ZBX_SERVER_HOST: 10.0.8.121
    ports:
      - 10050:10050
    extra_hosts:
      - "cdhuat01.weichai.com:10.0.11.91"
networks:
  default:
    external:
      name: net-12
volumes: 
  mysql:
  zabbix:
# 需自建网络
# docker network create --subnet=12.12.1.0/16 --gateway 12.12.1.1 net-12
# 默认密码 Admin/zabbix


docker run --name some-zabbix-agent --add-host=dockerhost:`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge` -p 10050:10050 -e ZBX_HOSTNAME="localhost" -e ZBX_SERVER_HOST=`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge`  -e ZBX_SERVER_PORT=10051 -d zabbix/zabbix-agent:alpine-3.2.5





＃ zabbix-server-mysql
docker run  --add-host=dockerhost:`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge` --name some-zabbix-server-mysql  -p 10051:10051 -e DB_SERVER_HOST=docker.for.mac.host.internal -e DB_SERVER_PORT=3306 -e MYSQL_USER="root" -e MYSQL_PASSWORD="admin" -d zabbix/zabbix-server-mysql:alpine-3.2.5

#  zabbix-web-apache-mysql

docker run --name some-zabbix-agent --add-host=dockerhost:`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge` -p 10050:10050 -e ZBX_HOSTNAME="localhost" -e ZBX_SERVER_HOST=`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge`  -e ZBX_SERVER_PORT=10051 -d zabbix/zabbix-agent:alpine-3.2.5

#  zabbix-agent

docker run --name some-zabbix-web-apache-mysql --add-host=dockerhost:`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge` -p 8088:80  -e DB_SERVER_HOST=docker.for.mac.host.internal -e DB_SERVER_PORT=3306 -e MYSQL_USER=root -e MYSQL_PASSWORD=admin -e ZBX_SERVER_HOST=`docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge` -e TZ="Asia/Shanghai" -d zabbix/zabbix-web-apache-mysql:alpine-3.2.5

# ps: 上述命令中的--add-host 和 docker.for.mac.host.internal 是为了解决docker访问本地数据库而添加的，如果IP地址固定的情况下，更改成相应的IP地址即可。

```

