## 软件安装

```shell
# 删除系统默认网络yum源配置
sudo rm -rf /etc/yum.repos.d/CentOS-*
# 安装潍柴yum源
sudo wget -O /etc/yum.repos.d/Centos7-WP.repo   http://mirrors.weichai.com/Centos7-WP.repo
# 安装docker软件
sudo yum -y install docker 
# 安装docker-compose
sudo yum -y install docker-compose
```

## 网段配置

```shell
sudo vim /etc/docker/daemon.json
# 添加以下配置
  "bip":"11.11.11.1/24",
  "registry-mirrors": ["http://10.0.8.121:1124"],
  "insecure-registries": ["10.0.8.121:1123"]  
```

## docker数据存放位置修改

```shell
# 原数据目录为/var/lib/docker
# sudo mv /var/lib/docker/ /home/docker
# 在ExecStart=/usr/bin/dockerd-current 后面,加入 -g /home/docker 参数
sudo vim /usr/lib/systemd/system/docker.service

ExecStart=/usr/bin/dockerd-current \
          -g /home/docker \
```

## 启动

```shell
sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl start docker
```

## 配置alias

```shell
sudo vim ~/.bashrc

alias docker='sudo docker '
alias docker-compose='sudo docker-compose '

source ~/.bashrc
```

