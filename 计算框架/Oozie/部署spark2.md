* 查看当前Oozie的share-lib共享库HDFS目录

  ```shell
  oozie admin -oozie http://10.0.11.22:11000/oozie -sharelibupdate
  ```

* 在Oozie的/user/oozie/share/lib/lib_20170921070424创建spark2目录

  ```shell
  sudo -u hdfs hadoop fs -mkdir /user/oozie/share/lib/lib_20181017105904/spark2
  hdfs dfs -mkdir /user/oozie/share/lib/lib_20181017105904/spark2
  ```

* 确认spark2已经添加到共享库

  ```shell
  oozie admin -oozie http://10.0.11.22:11000/oozie -shareliblist
  
  ```

  



