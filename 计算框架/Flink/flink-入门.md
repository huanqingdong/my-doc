* Flink-yarn-session

  ```
  ./bin/yarn-session.sh
  Usage:
     Required
       -n,--container <arg>   Number of YARN container to allocate (=Number of Task Managers)
     Optional
       -D <arg>                        Dynamic properties
       -d,--detached                   Start detached
       -jm,--jobManagerMemory <arg>    Memory for JobManager Container [in MB]
       -nm,--name                      Set a custom name for the application on YARN
       -q,--query                      Display available YARN resources (memory, cores)
       -qu,--queue <arg>               Specify YARN queue.
       -s,--slots <arg>                Number of slots per TaskManager
       -tm,--taskManagerMemory <arg>   Memory per TaskManager Container [in MB]
       -z,--zookeeperNamespace <arg>   Namespace to create the Zookeeper sub-paths for HA mode
  ```

  

```shell
./bin/yarn-session.sh -nm flink_faith -n 2 -jm 4096 -tm 1024  -s 1 -d

http://94.191.4.32/help.html
```

