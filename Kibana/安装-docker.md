## 拉取镜像

```shell
docker pull kibana/kibana:7.0.1

# 配置docker-compose.yml

 docker network create --subnet=11.11.1.0/24 --gateway 11.11.1.1 kibana




```

## docker-compose.yml

* https时需要指定extra_hosts,否则证书无效

```properties

version: '2.2'
services:
  kibana:
    container_name: kibana
    image: 10.0.8.121:1123/kibana:7.1.0
    environment:
      ELASTICSEARCH_HOSTS: https://cdhdev03.weichai.com:9200
      SERVER_SSL_ENABLED: 'true'
      SERVER_SSL_KEY: /usr/share/kibana/data/weichai.key
      SERVER_SSL_CERTIFICATE: /usr/share/kibana/data/weichai.pem
    ports:
      - 5601:5601
    networks:
      - esnet
    volumes:
      - ./kibana.keystore:/usr/share/kibana/data/kibana.keystore
      - ./weichai.key:/usr/share/kibana/data/weichai.key
      - ./weichai.pem:/usr/share/kibana/data/weichai.pem
    extra_hosts:
      - "cdhdev01.weichai.com:10.0.11.22"
      - "cdhdev02.weichai.com:10.0.11.23"
      - "cdhdev03.weichai.com:10.0.11.24"
      - "cdhdev04.weichai.com:10.0.11.25"
networks:
  esnet:
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      config:
      - subnet: 16.16.6.0/24

```

