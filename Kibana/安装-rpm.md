* 安装环境

  操作系统:  CentOS Linux release 7.2.1511 (Core) 

  kibana: 6.6.2


* 软件下载

  ```shell
  # 官网下载
  wget https://artifacts.elastic.co/downloads/kibana/kibana-6.6.2-x86_64.rpm
  wget https://artifacts.elastic.co/downloads/kibana/kibana-6.6.2-x86_64.rpm.sha512
  # 自建http服务器
  wget http://10.0.11.22/elk/kibana-6.6.2-x86_64.rpm
  wget http://10.0.11.22/elk/kibana-6.6.2-x86_64.rpm.sha512
  # 安装sha工具,并验证文件sha值, kibana-6.6.2-x86_64.rpm: OK表示文件无误
  yum install -y perl-Digest-SHA
  shasum -a 512 kibana-6.6.2-x86_64.rpm -c kibana-6.6.2-x86_64.rpm.sha512 
  ```

* 安装

  ```shell
  rpm -ivh kibana-6.6.2-x86_64.rpm
  
  ```

* 配置

  ```shell
  vim /etc/kibana/kibana.yml
  # 设置ip地址
  server.host: "10.0.11.22"
  # es服务地址
  elasticsearch.hosts: ["http://10.0.11.24:9200"]
  
  # 日志位置
  logging.dest: /var/log/kibana/kibana.log
  ```

* 启动

  ```shell
  # 开机启动
  systemctl daemon-reload
  systemctl enable kibana.service
  # 启动与关闭, .service可不加
  systemctl start kibana.service
  systemctl stop kibana.service
  # 查看状态
  systemctl status kibana
  ```

  

* ### Directory layout of RPM

  The RPM places config files, logs, and the data directory in the appropriate locations for an RPM-based system:

  | Type         | Description                                                  | Default Location             | Setting     |
  | ------------ | ------------------------------------------------------------ | ---------------------------- | ----------- |
  | **home**     | Kibana home directory or `$KIBANA_HOME`                      | `/usr/share/kibana`          |             |
  | **bin**      | Binary scripts including `kibana` to start the Kibana server and `kibana-plugin` to install plugins | `/usr/share/kibana/bin`      |             |
  | **config**   | Configuration files including `kibana.yml`                   | `/etc/kibana`                |             |
  | **data**     | The location of the data files written to disk by Kibana and its plugins | `/var/lib/kibana`            | `path.data` |
  | **optimize** | Transpiled source code. Certain administrative actions (e.g. plugin install) result in the source code being retranspiled on the fly. | `/usr/share/kibana/optimize` |             |
  | **plugins**  | Plugin files location. Each plugin will be contained in a subdirectory. | `/usr/share/kibana/plugins`  |             |



