

### 格式说明

| Format modifier | Left justify | Minimum width | Maximum width | Comment                                                      |
| --------------- | ------------ | ------------- | ------------- | ------------------------------------------------------------ |
| %20logger       | false        | 20            | none          | Left pad with spaces if the logger name is less than 20 characters long. |
| %-20logger      | true         | 20            | none          | Right pad with spaces if the logger name is less than 20 characters long. |
| %.30logger      | NA           | none          | 30            | Truncate from the beginning if the logger name is longer than 30 characters. |
| %20.30logger    | false        | 20            | 30            | Left pad with spaces if the logger name is shorter than 20 characters. However, if logger name is longer than 30 characters, then truncate from the beginning. |
| %-20.30logger   | true         | 20            | 30            | Right pad with spaces if the logger name is shorter than 20 characters. However, if logger name is longer than 30 characters, then truncate from the *beginning*. |
| %.-30logger     | NA           | none          | 30            | Truncate from the *end* if the logger name is longer than 30 characters. |

### 示例

| Format modifier | Logger name           | Result         |
| --------------- | --------------------- | -------------- |
| [%20.20logger]  | main.Name             | `[ main.Name]` |
| [%-20.20logger] | main.Name             | `[main.Name ]` |
| [%10.10logger]  | main.foo.foo.bar.Name | `[o.bar.Name]` |
| [%10.-10logger] | main.foo.foo.bar.Name | `[main.foo.f]` |

```xml

<?xml version="1.0" encoding="UTF-8"?>
<configuration
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="http://www.padual.com/java/logback.xsd"
        debug="false" scan="true" scanPeriod="30 second">

    <springProperty name="topic" source="log.kafka.topic" defaultValue="topic"/>
    <springProperty name="system" source="log.kafka.system" defaultValue="system"/>
    <springProperty name="bootstrap-servers" source="log.kafka.bootstrap-servers"/>
    <springProperty name="security-protocol" source="log.kafka.security-protocol"/>
    <springProperty name="sasl-jaas-config" source="log.kafka.sasl-jaas-config"/>
    <springProperty name="application-name" source="spring.application.name"/>
    <springProperty name="profiles-active" source="spring.profiles.active"/>


    <property name="pattern" value="%d %5p %20.20t [%X{TID}] %m [%c] %n"/>

    <!-- 控制台打印 -->
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder charset="utf-8">
            <pattern>${pattern}</pattern>
        </encoder>
    </appender>

    <!-- 根据是否存在bootstrap-servers来开启kerberos -->
    <if condition='!isNull("bootstrap-servers")'>
        <then>
            <appender neverBlock="true" name="KafkaAppender"
                      class="com.github.danielwegener.logback.kafka.KafkaAppender">
                <encoder class="com.github.danielwegener.logback.kafka.encoding.LayoutKafkaMessageEncoder">
                    <layout class="net.logstash.logback.layout.LogstashLayout">
                        <includeMdcKeyName>TID</includeMdcKeyName>
                        <!-- <includeContext>true</includeContext>
                        <includeCallerData>true</includeCallerData> -->
                        <customFields>{"system":"${system}"}</customFields>
                        <fieldNames class="net.logstash.logback.fieldnames.ShortenedFieldNames"/>
                    </layout>
                </encoder>
                <topic>${topic}</topic>
                <keyingStrategy class="com.github.danielwegener.logback.kafka.keying.HostNameKeyingStrategy"/>
                <deliveryStrategy class="com.github.danielwegener.logback.kafka.delivery.AsynchronousDeliveryStrategy"/>
                <producerConfig>bootstrap.servers=${bootstrap-servers}</producerConfig>
                <!-- 如果配置了sasl-jaas-config属性,则使用kerberos认证 -->
                <if condition='!isNull("sasl-jaas-config")'>
                    <then>
                        <producerConfig>security.protocol=SASL_PLAINTEXT</producerConfig>
                        <producerConfig>sasl.jaas.config=${sasl-jaas-config}</producerConfig>
                    </then>
                </if>
            </appender>

            <!-- 异步输出 -->
            <appender name="ASYNC" class="ch.qos.logback.classic.AsyncAppender">
                <appender-ref ref="KafkaAppender"/>
                <neverBlock>true</neverBlock>
            </appender>
        </then>
    </if>

    <!-- SQL相关日志输出-->
    <logger name="org.springframework.jdbc.core.JdbcTemplate" level="DEBUG"/>
    <logger name="org.springframework.jdbc.core.StatementCreatorUtils" level="Trace"/>
    <logger name="org.apache.kafka" level="WARN"/>
    <logger name="com.weichai.mapper" level="DEBUG"/>

    <root level="INFO">
        <appender-ref ref="STDOUT"/>
        <if condition='!isNull("bootstrap-servers")'>
            <then>
                <appender-ref ref="ASYNC"/>
            </then>
        </if>
    </root>

</configuration>

```

### 带文件版本

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="http://www.padual.com/java/logback.xsd"
        debug="false" scan="true" scanPeriod="30 second">

<springProperty name="topic" source="log.kafka.topic" defaultValue="topic"/>
<springProperty name="system" source="log.kafka.system" defaultValue="system"/>
<springProperty name="bootstrap-servers" source="log.kafka.bootstrap-servers"/>
<springProperty name="security-protocol" source="log.kafka.security-protocol"/>
<springProperty name="sasl-jaas-config" source="log.kafka.sasl-jaas-config"/>
<springProperty name="application-name" source="spring.application.name"/>
<springProperty name="base-path" source="log.base-path"/>
<springProperty name="profiles-active" source="spring.profiles.active"/>

<property name="pattern" value="%d %5p %20.20t [%X{TID}] %m [%c] %n"/>

<!-- 控制台打印 -->
<if condition='property("profiles-active").contains("dev")'>
    <then>
        <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
            <encoder charset="utf-8">
                <pattern>${pattern}</pattern>
            </encoder>
        </appender>
    </then>
</if>

<!-- 文件日志 -->
<appender name="rollingFileAppender" class="ch.qos.logback.core.rolling.RollingFileAppender">
    <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
        <fileNamePattern>${base-path}/${system}/%d{yyyy-MM-dd}.log
        </fileNamePattern>
        <maxHistory>10</maxHistory>
    </rollingPolicy>
    <encoder>
        <pattern>${pattern}</pattern>
    </encoder>
    <!-- 文件不打印debug级别日志 -->
    <filter class="ch.qos.logback.classic.filter.LevelFilter">
        <level>DEBUG</level>
        <onMatch>DENY</onMatch>
        <onMismatch>ACCEPT</onMismatch>
    </filter>
</appender>

<!-- 根据是否存在bootstrap-servers来开启kerberos -->
<if condition='!isNull("bootstrap-servers")'>
    <then>
        <appender neverBlock="true" name="KafkaAppender"
                  class="com.github.danielwegener.logback.kafka.KafkaAppender">
            <encoder class="com.github.danielwegener.logback.kafka.encoding.LayoutKafkaMessageEncoder">
                <layout class="net.logstash.logback.layout.LogstashLayout">
                    <includeMdcKeyName>TID</includeMdcKeyName>
                    <!-- <includeContext>true</includeContext>
                    <includeCallerData>true</includeCallerData> -->
                    <customFields>{"system":"${system}"}</customFields>
                    <fieldNames class="net.logstash.logback.fieldnames.ShortenedFieldNames"/>
                </layout>
            </encoder>
            <topic>${topic}</topic>
            <keyingStrategy class="com.github.danielwegener.logback.kafka.keying.HostNameKeyingStrategy"/>
            <deliveryStrategy class="com.github.danielwegener.logback.kafka.delivery.AsynchronousDeliveryStrategy"/>
            <producerConfig>bootstrap.servers=${bootstrap-servers}</producerConfig>
            <!-- 如果配置了sasl-jaas-config属性,则使用kerberos认证 -->
            <if condition='!isNull("sasl-jaas-config")'>
                <then>
                    <producerConfig>security.protocol=SASL_PLAINTEXT</producerConfig>
                    <producerConfig>sasl.jaas.config=${sasl-jaas-config}</producerConfig>
                </then>
            </if>
        </appender>

        <!-- 异步输出 -->
        <appender name="ASYNC" class="ch.qos.logback.classic.AsyncAppender">
            <appender-ref ref="KafkaAppender"/>
            <neverBlock>true</neverBlock>
        </appender>
    </then>
</if>

<!-- SQL相关日志输出-->
<logger name="org.springframework.jdbc.core.JdbcTemplate" level="DEBUG"/>
<logger name="org.springframework.jdbc.core.StatementCreatorUtils" level="Trace"/>
<logger name="org.apache.kafka" level="WARN"/>
<logger name="com.weichai" level="DEBUG"/>

<root level="INFO">
    <if condition='property("profiles-active").contains("dev")'>
        <then>
            <appender-ref ref="STDOUT"/>
        </then>
    </if>
    <appender-ref ref="rollingFileAppender"/>
    <if condition='!isNull("bootstrap-servers")'>
        <then>
            <appender-ref ref="ASYNC"/>
        </then>
    </if>
</root>
</configuration>
```



