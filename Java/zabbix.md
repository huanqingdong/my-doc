## rpm包下载

```
https://repo.zabbix.com/zabbix/4.2/rhel/7/x86_64/
```

## 安装

```shell
rpm -ivh http://10.0.11.22/soft/zabbix-agent-4.2.1-1.el7.x86_64.rpm

yum -y install http://10.0.11.22/soft/zabbix-agent-4.2.1-1.el7.x86_64.rpm

systemctl enable zabbix-agent.service
systemctl start zabbix-agent.service
```

