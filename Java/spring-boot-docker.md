## 简介

使用maven插件构建springboot项目的docker镜像,插件github地址:https://github.com/spotify/docker-maven-plugin

## 环境配置

默认情况下插件会连接的docker地址为:localhost:2375, 可以通过设置环境变量DOCKER_HOST来改变连接docker的地址

## MAVEN配置

> >  dockerDirectory目录下的内容复制到 `${project.build.directory}/docker`下, 使用`resources` 标签复制额外的内容, 例如 service's jar file.

```xml
<build>
  <plugins>
    ...
    <plugin>
      <groupId>com.spotify</groupId>
      <artifactId>docker-maven-plugin</artifactId>
      <version>1.2.0</version>
      <configuration>
          <imageName>10.0.8.121:1123/${project.artifactId}</imageName>
          <dockerDirectory>docker</dockerDirectory>
          <serverId>docker-local</serverId>
          <registryUrl>http://10.0.8.121:1123</registryUrl>
          <imageTags>
              <imageTag>${project.version}</imageTag>
              <imageTag>latest</imageTag>
          </imageTags>
          <resources>
              <resource>
                  <targetPath>/</targetPath>
                  <directory>${project.build.directory}</directory>
                  <include>${project.build.finalName}.jar</include>
              </resource>
              <!-- 复制证书 -->
              <resource>
                  <targetPath>/</targetPath>
                  <directory>${project.build.directory}</directory>
                  <include>test-classes/*.txt</include>
              </resource>
          </resources>
      </configuration>
    </plugin>
    ...
  </plugins>
</build>
​```
```

## Dockerfile

```shell
FROM openjdk:8u212
# 创建tmp挂载点
VOLUME /tmp

# 创建路径
RUN mkdir -p /home/dft/duty-determine

# 添加测试,训练数据
ADD test-classes/train_data.txt /home/dft/duty-determine/train_data.txt
ADD test-classes/test_data.txt /home/dft/duty-determine/test_data.txt

# 添加jar包
ADD duty-determine.jar app.jar
# 更改时间
RUN sh -c 'touch /app.jar'

# 对外暴露JAVA_OPTS,PARAMS两个环境变量
ENV JAVA_OPTS="-Xms2g -Xmx2g"
ENV PARAMS=""
ENTRYPOINT ["sh","-c","java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar  $PARAMS"]
```

## docker-compose.yml

```shell
version: "2.2"
services:
  duty-determine:
    image: 10.0.8.121:1123/duty-determine
    container_name: duty-determine1
    hostname: duty-determine1
    privileged: true
    environment:
      PARAMS: --spring.profiles.active=dev
      JAVA_OPTS: -Xms2G -Xmx2G
    ports:
      - 8080:8080
    extra_hosts:
      - "cdhuat01.weichai.com:10.0.11.91"
      - "cdhuat02.weichai.com:10.0.11.92"
      - "cdhuat03.weichai.com:10.0.11.93"
      - "cdhuat04.weichai.com:10.0.11.94"
      - "cdhdev01.weichai.com:10.0.11.22"
      - "cdhdev02.weichai.com:10.0.11.23"
      - "cdhdev03.weichai.com:10.0.11.24"
      - "cdhdev04.weichai.com:10.0.11.25"
      - "SmartSearch-es01.weichai.com:10.0.1.221"
      - "SmartSearch-es02.weichai.com:10.0.1.222"
      - "SmartSearch-es03.weichai.com:10.0.1.223"
      - "SmartSearch-es04.weichai.com:10.0.1.224"
    networks:
      net:
networks:
  net:
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      config:
        - subnet: 12.12.12.0/24

```

## 认证

* pom.xml

```xml
    <plugin>
      <plugin>
        <groupId>com.spotify</groupId>
        <artifactId>docker-maven-plugin</artifactId>
        <version>VERSION GOES HERE</version>
        <configuration>
          [...]
          <serverId>docker-local</serverId>
          <registryUrl>http://10.0.8.121:1123</registryUrl>
        </configuration>
      </plugin>
    </plugins>
```

* [maven密码](https://maven.apache.org/guides/mini/guide-encryption.html)

  ```shell
  # 创建master密码
  mvn --encrypt-master-password 123456a?
  # 将上一步产生的密码保存到 ${user.home}/.m2/settings-security.xml; 格式如下
  <settingsSecurity>
    <master>{Si1aC4q9Z2AHtDuvV8VwH2EY1oRpCRydQuSsvc/qy28=}</master>
  </settingsSecurity>
  # 加密一个server密码
  mvn --encrypt-password 123456a?*
  mvn --ep 123456a?*
  {EyXCBpcrQmsHFozhBIUDuyVWS8uG9HoUR7s7vMcGycs=}
  ```

* setting.xml

  ```xml
  </servers>	
      <server>
          <id>docker-local</id>
          <username>huanqingdong</username>
          <password>{EyXCBpcrQmsHFozhBIUDuyVWS8uG9HoUR7s7vMcGycs=}</password>
          <configuration>
              <email>huanqingdong@weichai.com</email>
          </configuration>
      </server>
  </servers>
  ```

  

## 命令

```shell
# build 
mvn clean package docker:build
# push
mvn clean package docker:build -DpushImageTag

```

