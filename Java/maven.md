* 跳过测试

  ```xml
  <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-surefire-plugin</artifactId>
      <version>2.18.1</version>
      <configuration>
          <skipTests>true</skipTests>
      </configuration>
  </plugin>
  ```

  