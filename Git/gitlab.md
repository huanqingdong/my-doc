* 设置gitlab clone项目时的地址

  ```shell
  vim /opt/gitlab/embedded/service/gitlab-rails/config/gitlab.yml
  ```

* 集成LDAP

  修改配置文件 vim /etc/gitlab/gitlab.rb

  ```properties
  gitlab_rails['ldap_enabled'] = true
  
  ###! **remember to close this block with 'EOS' below**
  gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
     main: # 'main' is the GitLab 'provider ID' of this LDAP server
  #     label: 'LDAP'
        host: '10.0.6.101'
        port: 389
        uid: 'sAMAccountName'
        bind_dn: 'CN=s-plan,OU=服务帐户,OU=服务帐户,DC=weichai,DC=com'
        password: '123456a?'
        encryption: 'plain' # "start_tls" or "simple_tls" or "plain"
  #     verify_certificates: true
  #     smartcard_auth: false
        active_directory: true
        allow_username_or_email_login: false
  #     lowercase_usernames: false
  #     block_auto_created_users: false
        base: 'DC=weichai,DC=com'
        user_filter: ''
  #     ## EE only
  #     group_base: ''
  #     admin_group: ''
  #     sync_ssh_keys: false
  #
  #   secondary: # 'secondary' is the GitLab 'provider ID' of second LDAP server
  #     label: 'LDAP'
  #     host: '_your_ldap_server'
  #     port: 389
  #     uid: 'sAMAccountName'
  #     bind_dn: '_the_full_dn_of_the_user_you_will_bind_with'
  #     password: '_the_password_of_the_bind_user'
  #     encryption: 'plain' # "start_tls" or "simple_tls" or "plain"
  #     verify_certificates: true
  #     smartcard_auth: false
  #     active_directory: true
  #     allow_username_or_email_login: false
  #     lowercase_usernames: false
  #     block_auto_created_users: false
  #     base: ''
  #     user_filter: ''
  #     ## EE only
  #     group_base: ''
  #     admin_group: ''
  #     sync_ssh_keys: false
  EOS
  ```

  重新配置gitlab

  ```shell
  gitlab-ctl reconfigure
  # 日志查看
  sudo gitlab-ctl tail
  ```

  ## 备份迁移

  ```shell
  # 备份命令
  gitlab-rake gitlab:backup:create
  
  ```

  ## 常用命令

  ```shell
  # 版本查看
  gitlab-rake gitlab:env:info
  ```

  

  ## docker安装

  ```shell
  # cp: cannot create regular file '/etc/gitlab/gitlab.rb': Permission denied
  # change file SELinux security context
  chcon -Rt svirt_sandbox_file_t /srv/gitlab/config
  chcon -Rt svirt_sandbox_file_t /srv/gitlab/logs
  chcon -Rt svirt_sandbox_file_t /srv/gitlab/data
  
  
  ```

  