## 配置

```shell
# 设置全局用户名,邮件
git config --global user.name huanqingdong
git config --global user.email 609139528@qq.com
# 查看全局配置
git config --global --list
# ssh免密登入配置
ssh-keygen -t rsa
# 按3个回车，执行完成后会在C:\Users\Lenovo\.ssh下生成id_rsa,id_rsa.pub两个文件
# 将id_rsa.pub中的内容配置到github
# 添加完成后,执行以下命令验证是否能连接成功
ssh git@github.com

```



```shell
 # 删除github上的commit
 git push origin HEAD --force
```



