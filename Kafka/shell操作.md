## 查看主题副本信息

```shell
./kafka-topics.sh --describe --zookeeper localhost:2181 --topic es_search_log

/opt/cloudera/parcels/KAFKA-2.2.0-1.2.2.0.p0.68/lib/kafka/bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic es_search_log


# 查看主题
kafka-topics --describe --zookeeper localhost:2181 --topic es_search_log
# 删除主题
kafka-topics --delete --zookeeper localhost:2181 --topic log_es_test
```

