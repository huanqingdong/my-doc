## .jks -> .p12
```bash
keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.p12 -srcstoretype jks -deststoretype pkcs12
```

## 生成key 加密的pem证书:
```bash
openssl pkcs12 -in keystore.p12 -out keystore.pem
```
## 生成key 非加密的pem证书:
```bash
openssl pkcs12 -nodes -in keystore.p12 -out keystore-nodes.pem
```

## 提取证书部分-> xx.pem
```bash
Bag Attributes
....
-----BEGIN CERTIFICATE-----
....
-----END CERTIFICATE-----
```

## 提取私钥部分-> xx.key
```bash
Bag Attributes
....
-----BEGIN CERTIFICATE-----
....
-----END CERTIFICATE-----
```

## 私钥去除密码
```bash
openssl rsa -in test.key -out test-nodes.key
```
