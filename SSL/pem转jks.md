##  pem+key->p12

输入pem+key,输出p12
```shell
openssl pkcs12 -export -out test.p12 -in test.pem -inkey test.key
```

## p12->jks

```shell
keytool -importkeystore -srckeystore test.p12 -srcstoretype PKCS12 -destkeystore test.jks  -deststoretype JKS
```

## jks查看

```shell
keytool -list -v -keystore test.jks
```

