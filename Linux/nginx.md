## 安装

```shell
#wget 'http://nginx.org/download/nginx-1.13.6.tar.gz'
#wget 'https://codeload.github.com/openresty/headers-more-nginx-module/tar.gz/v0.33'
# 文件清理
if [ -f "nginx-1.13.6.tar.gz" ];then
	rm -rf nginx-1.13.6.tar.gz
	echo '清理nginx-1.13.6.tar.gz'
fi
if [ -f "headers-more-nginx-module-0.33.tar.gz" ];then
	rm -rf headers-more-nginx-module-0.33.tar.gz
	echo '清理headers-more-nginx-module-0.33.tar.gz'
fi
# 目录清理
if [ -d "nginx-1.13.6" ];then
	rm -rf nginx-1.13.6
	echo '清理nginx-1.13.6'
fi
if [ -d "headers-more-nginx-module-0.33" ];then
	rm -rf headers-more-nginx-module-0.33
	echo '清理headers-more-nginx-module-0.33'
fi
echo '清理结束..' && sleep 1
# 下载安装包
wget 'http://10.0.6.122/software/nginx-1.13.6.tar.gz'
wget 'http://10.0.6.122/software/headers-more-nginx-module-0.33.tar.gz'
# 解压
tar -xzvf nginx-1.13.6.tar.gz
tar -xzvf headers-more-nginx-module-0.33.tar.gz
echo '下载,解压完成..' && sleep 1 
# 进入nginx-home编译
cd nginx-1.13.6/

# Here we assume you would install you nginx under /opt/nginx/.
./configure --prefix=/opt/nginx \
     --add-module=../headers-more-nginx-module-0.33 \
     --with-http_ssl_module \
&& echo '配置完成..' && sleep 1
# 安装
make && echo '编译完成..' && sleep 1 \
&& make install && echo '安装完成..' && sleep 1

```

## 检查存活

```shell
#!/bin/bash

ROW_CNT=`ps -ef |grep 'nginx: master process'|wc -l`
# 1行没启动
if [[ "$ROW_CNT" == "1" ]]
   then
      echo "nginx is not running"
      killall keepalived
    else
      echo "nginx is running"
fi
```
## keepalive

```shell
# 软件安装
yum -y install keepalived
# 设置开机启动
systemctl enable keepalived
vim /etc/keepalived/keepalived.conf

systemctl start keepalived
systemctl stop keepalived
systemctl restart keepalived

systemctl status keepalived
```

* MASTER

  > virtual_router_id:同一段内注意不能冲突,最大取值255,同一集群内值相同
  >
  > interface:网卡名称,通过`ip a`查看
  >
  > vim /etc/keepalived/keepalived.conf

```properties
! Configuration File for keepalived

global_defs {
}
vrrp_script check_nginx {
  script "/root/faith/nginx/check_nginx.sh"
  interval 2
}

vrrp_instance VI_1 {
    state MASTER
    interface enp134s0f0
    virtual_router_id 126
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        10.0.11.20
    }
    track_script {
        check_nginx
    }
}
```

* BACKUP

```properties
! Configuration File for keepalived

global_defs {
}
vrrp_script check_nginx {
  script "/root/faith/nginx/check_nginx.sh"
  interval 2
}

vrrp_instance VI_1 {
    state BACKUP
    interface enp134s0f0
    virtual_router_id 126
    priority 90
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
       10.0.11.20
    }
    track_script {
        check_nginx
    }
}

```




* 配置相关

```
# 中文乱码,server段内加入charset utf-8;
```

* 负载均衡

  ```shell
  https://www.cnblogs.com/handongyu/p/6410405.html
  ```

  