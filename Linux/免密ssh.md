* 生成ssh密钥文件

  本地系统执行 ssh-keygen -t rsa 命令，生成密钥文件

  ```shell
  # 生成密钥对
  ssh-keygen -t rsa
  # 默认在~/.ssh路径下
  # id_rsa 私钥文件
  # id_rsa.pub 公钥文件
  ```

* 将公钥发送到目标机器

  本地机器执行命令如：ssh-copy-id -i ~/.ssh/id_rsa.pub dss@10.164.194.42， 将公钥文件传输的远程机器，并生效

  ```shell
  ssh-copy-id -i ~/.ssh/id_rsa.pub elk@elk02
  ```

  