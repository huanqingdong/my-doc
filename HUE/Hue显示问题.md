* hdfs列表日期

  ```shell
  vim /opt/cloudera/parcels/CDH-5.13.3-1.cdh5.13.3.p0.2/lib/hue/apps/filebrowser/src/filebrowser/views.py
  # 修改日期格式
  'mtime': datetime.fromtimestamp(stats['mtime']).strftime('%Y-%m-%d %H:%M:%S') if stats['mtime'] else '',
  ```

  

* hdfs单文件日期

  ```shell
  vim /opt/cloudera/parcels/CDH-5.13.3-1.cdh5.13.3.p0.2/lib/hue/desktop/core/src/desktop/templates/common_header_footer_components.mako
  # moment.utc(mTime).format("L LT")改为moment(mTime).format("L LT");
  return moment(mTime).format("L LT"); 
  ```
