## 
```sql
-- 创建临时表空间 
create temporary tablespace tbs_search_temp --test_temp表空间名称
tempfile '/home/app/oradata/smartsearch/tbs_search_temp.dbf'--oracle文件路径
size 1024m 
autoextend on 
next 128m maxsize UNLIMITED 
extent management local; 
--创建数据表空间 
create tablespace tbs_search
logging 
datafile '/home/app/oradata/smartsearch/tbs_search.dbf'--oracle文件路径
size 1024m --初始大小
autoextend on     --自动扩展
next 128m maxsize UNLIMITED --一次扩展大小,最大大小
extent management local; 

--创建用户并指定表空间 
create user search identified by 123456 --username用户名称
default tablespace tbs_search --默认用户表空间
temporary tablespace tbs_search_temp; --默认临时表空间

--给用户授予权限 
grant connect,resource to search; 
grant dba to search

--删除空的表空间，但是不包含物理文件
drop tablespace tablespace_name;
--删除非空表空间，但是不包含物理文件
drop tablespace tablespace_name including contents;
--删除空表空间，包含物理文件
drop tablespace tablespace_name including datafiles;
--删除非空表空间，包含物理文件
drop tablespace tablespace_name including contents and datafiles;
--如果其他表空间中的表有外键等约束关联到了本表空间中的表的字段，就要加上CASCADE CONSTRAINTS
drop tablespace tablespace_name including contents and datafiles CASCADE CONSTRAINTS;

--说明： 删除了user，只是删除了该user下的schema objects，是不会删除相应的tablespace的。
drop user username cascade
```





